

/**
 * @author nhphung
 */
package bkool;

import java.io.*;
import org.antlr.v4.runtime.ANTLRFileStream;
import bkool.parser.*;


public class Main  {
  
  static final String sepa = "//" ;// dung cho linux
  static final String phase1 = "testphase1";
  static final String phase2 = "testphase2";

  public static void main(String[] args) throws IOException {
  	String option,indirphase1,outdirphase1;
  	int startphase1,endphase1;
    if (args.length == 5) {
      option = args[0].substring(1);
      startphase1 = Integer.parseInt(args[1]);
      endphase1 = Integer.parseInt(args[2]);
      indirphase1 = args[3];
      outdirphase1 = args[4];

     runTest(option,startphase1,endphase1,indirphase1,outdirphase1);

      }
    
    else  System.out.println("Usage: java bkool.Main -phase1 start end indir outdir ");
  }
  
  public static void runTest(String opt,int start,int end,String indir,String outdir) 
  throws IOException,FileNotFoundException {
    ANTLRFileStream source;
    PrintWriter dest;
    for (int i = start; i <= end; i++) {     
      System.out.println("Test "+i);
      source = new ANTLRFileStream(indir+sepa+i+".txt");
      dest = new PrintWriter(new File(outdir+sepa+i+".txt"));
      if (opt.equals("phase1"))
      	TestLexer.test(source,dest);
      else if (opt.equals("phase2"))
      	TestParser.test(source,dest);
      else
      	throw new ClassCastException();
      dest.close();
    }
  } 
}