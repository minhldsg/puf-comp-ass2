

/**
 * @author nhphung
 */
package bkool.parser;
import org.antlr.v4.runtime.BaseErrorListener;
import java.io.PrintWriter;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;

public class ProcessError {
    
    static BaseErrorListener createErrorListener() {
     	return new BaseErrorListener() {
     		@Override
            public void  syntaxError(Recognizer<?, ?> recognizer, Object obj, int line, int position, String message, RecognitionException ex) {
              throw new IllegalArgumentException("Error on line " +line+" col "+ (position + 1));// +" : "+ ((Token)obj).getText()) ;
            }          
        };
    }
}

