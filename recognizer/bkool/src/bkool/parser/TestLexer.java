

/**
 * @author nhphung
 */
package bkool.parser;
import org.antlr.v4.runtime.Token;
import java.io.*;
import org.antlr.v4.runtime.ANTLRFileStream;

public class TestLexer {
  
  public static void main(String args[]) 
  throws IOException,FileNotFoundException {
    String file,out;
    ANTLRFileStream infile;
    PrintWriter outFile;
    file = (args.length > 0)? args[0] : "test.txt";
    infile = new ANTLRFileStream(file);
    out = (args.length > 1)? args[1] : "output.txt"; 
    outFile = new PrintWriter(new File(out));
    test(infile,outFile);
    outFile.close();
  }
  public static void test(ANTLRFileStream infile,PrintWriter outfile) {
    BKOOLLexer lexer;
    lexer = new BKOOLLexer(infile);
      
      try {
        printLexeme(lexer,outfile);
      }
      catch (ErrorToken et){
        outfile.println("ErrorToken "+et.s);
      }
      catch (IllegalEscape ie){
        outfile.println("Illegal escape in string: "+ie.s);   
      }
      catch (UncloseString us) {
        outfile.println("Unclosed string: "+ us.s);
      }
      catch (UnterminatedComment uc) {
        outfile.println("Unterminated Comment");
      }
  }
  static void printLexeme(BKOOLLexer lexer,PrintWriter dev){
    
      Token tok = lexer.nextToken();
      if (tok.getType() != Token.EOF) {
        dev.println(tok.getText());
        printLexeme(lexer,dev);
      } else dev.print(tok.getText());
    
    
  }
  
  

}