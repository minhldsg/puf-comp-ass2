

/**
 * @author nhphung
 */
package bkool.parser;

import java.io.*;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.BaseErrorListener;




//import ProgramContext;

public class TestParser {
    
/*
  def main(args:List[String]) {
    if (args.length == 2) {
      val inputFile = new ANTLRFileStream(args(0))
      val out = new PrintWriter(new File(args(1)));
      try {
        test(inputFile,out);
      }    
      catch  {
        case e:Exception => out.println(e.getMessage());
      }
      finally {
        out.close()
      }
    } 
    else 
        println("Usage: scala TestParser <input file> <output file>")
  }*/

    /**
     * Parses an expression into an integer result.
     * @param expression the expression to part
     * @return and integer result
     */
    public static void test(ANTLRFileStream fileName,PrintWriter outFile) {
//    def test(fileName:ANTLRFileStream,outFile:PrintWriter) = {
        /*
         * Create a lexer that reads from our expression string
         */
        BKOOLLexer lexer;
        lexer = new BKOOLLexer(fileName); // ANTLRInputStream
        BaseErrorListener _listener = ProcessError.createErrorListener();
        

        /*
         * The lexer reads characters and lexes them into token stream. The
         * tokens are consumed by the parser that then builds an Abstract
         * Syntax Tree.
         */
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        BKOOLParser parser = new BKOOLParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(_listener);
    try {
      parser.program();
      outFile.println("sucessful");
    } catch (Exception e){
      outFile.println(e.getMessage());
    }
    
    }
}