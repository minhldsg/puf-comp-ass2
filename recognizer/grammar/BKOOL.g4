/**
 * Student name: Le Duy Minh
 * Student ID/Email: minhld.sg@gmail.com
 */
grammar BKOOL;

@lexer::header{
    package bkool.parser;
}

@lexer::members{
@Override
public Token emit() {
    switch (getType()) {
        case ILLEGAL_ESCAPE:
            Token result = super.emit();
            throw new IllegalEscape(result.getText());

        case UNCLOSE_STRING:
            result = super.emit();
            throw new UncloseString(result.getText());

        case ERROR_TOKEN:
            result = super.emit();
            throw new ErrorToken(result.getText());

        case UNTERMINATED_COMMENT:
            result = super.emit();
            throw new UnterminatedComment();

        case UNTERMINATED_COMMENT_INLINE:
            result = super.emit();
            throw new UnterminatedComment();

        default:
            return super.emit();
        }
    }
}

@parser::header{
	package bkool.parser;
}

options{
	language=Java;
}

// Ass2: Grammar
program: KEY_CLASS ID (KEY_EXTENDS super_class)? class_body;
super_class: ID;
class_body: SPT_LP members SPT_RP;
members: (mod* member_decl)*;
mod: KEY_STATIC | KEY_FINAL;
member_decl: method_decl | variable_decl | constant_decl;
constant_decl: KEY_STATIC? KEY_FINAL type ID OPT_ASSIGN_CONST expr SPT_SM;
variable_decl: KEY_STATIC? ids SPT_COL type SPT_SM;
method_decl: type KEY_STATIC? ID SPT_LB params SPT_RB block_stmt;
params: param | param SPT_COM params;
param: ID SPT_COL type;
block_stmt: SPT_LP (local_var_decl | stmt)* SPT_RP;
local_var_decl: ids SPT_COL type SPT_SM;
stmt: (KEY_THIS SPT_DOT)? ID bop=OPT_ASSIGN_VARIABLE expr SPT_SM
    | expr SPT_SM
    | expr SPT_LQB expr SPT_RQB OPT_ASSIGN_VARIABLE expr SPT_SM
    | KEY_IF expr KEY_THEN stmt (KEY_ELSE stmt)?
    | KEY_FOR for_var_decl (KEY_TO | KEY_DOWNTO) expr KEY_DO stmt
    | KEY_BREAK SPT_SM
    | KEY_CONTINUE SPT_SM
    | KEY_RETURN expr? SPT_SM
    | ID SPT_DOT ID params SPT_SM
    ;
for_var_decl: ID OPT_ASSIGN_VARIABLE expr;
type: type_pri | type_class;
type_pri: KEY_INT | KEY_FLOAT | KEY_BOOLEAN | KEY_STRING | KEY_VOID | KEY_ARRAY;
type_class: ID;
ids: ID (SPT_COM ID)*;
call_method: ID SPT_LB exprs? SPT_RB;
exprs: expr (SPT_COM expr)*;
expr: primary
    | expr bop=SPT_DOT (ID | call_method)
    | expr bop=(OPT_ADD | OPT_SUB | OPT_MUL | OPT_FLOAT_DIV | OPT_INT_DIV | OPT_MOD) expr
    | expr bop=(OPT_AND | OPT_OR | OPT_BANG) expr
    | expr (OPT_EQUAL | OPT_NOTEQUAL | OPT_GT | OPT_LT | OPT_GE | OPT_LE) expr
    | expr bop=OPT_CARET expr
    | expr SPT_LQB expr SPT_RQB
    | call_method
    | <assoc=right> KEY_NEW type_class SPT_LB exprs? SPT_RB
    | <assoc=right> prefix=(OPT_ADD | OPT_SUB | OPT_BANG) expr
    ;
primary: SPT_LB expr SPT_RB | KEY_THIS | INTERGER_LITERAL | FLOAT_LITERAL | CHAR_LITERAL | STRING_LITERAL | BOOLEAN_LITERAL | NULL_LITERAL | ID;

// Ass1: Lexer
// White spaces
WS : [ \t\r\n]+ -> skip;
// Comments
BC: '/*' .*? '*/' -> skip; // Block comment
LC: '%%' ~[\r\n]* -> skip; // Line comment
// Keywords
KEY_CLASS: 'class';
KEY_EXTENDS: 'extends';
KEY_STATIC: 'static';
KEY_THIS: 'this';
KEY_NEW: 'new';
KEY_FINAL: 'final';
KEY_VOID: 'void';
KEY_BOOLEAN: 'boolean';
KEY_STRING: 'string';
KEY_INT: 'int';
KEY_FLOAT: 'float';
KEY_FOR: 'for';
KEY_BREAK: 'break';
KEY_IF: 'if';
KEY_ELSE: 'else';
KEY_THEN: 'then';
KEY_CONTINUE: 'continue';
KEY_TRUE: 'true';
KEY_FALSE: 'false';
KEY_DO: 'do';
KEY_TO: 'to';
KEY_DOWNTO: 'downto';
KEY_RETURN: 'return';
KEY_NIL: 'nil';
KEY_ARRAY: 'array';
// Identifiers
ID: [a-zA-Z_]+[a-zA-Z_0-9]*;
// Operators
OPT_ADD: '+';
OPT_SUB: '-';
OPT_MUL: '*';
OPT_FLOAT_DIV: '/';
OPT_INT_DIV: '\\';
OPT_MOD: '%';
OPT_NOTEQUAL: '!=';
OPT_EQUAL: '==';
OPT_LT: '<';
OPT_LE: '<=';
OPT_GT: '>';
OPT_GE: '>=';
OPT_OR: '||';
OPT_AND: '&&';
OPT_BANG: '!';
OPT_CARET: '^';
OPT_ASSIGN_CONST: '=';
OPT_ASSIGN_VARIABLE: ':=';
// Separators
SPT_LP: '{';
SPT_RP: '}';
SPT_LB: '(';
SPT_RB: ')';
SPT_LQB: '[';
SPT_RQB: ']';
SPT_DOT: '.';
SPT_COM: ',';
SPT_SM: ';';
SPT_COL: ':';
// Literals
INTERGER_LITERAL: '0'|[1-9][0-9]*;
BOOLEAN_LITERAL: KEY_FALSE | KEY_TRUE;
FLOAT_LITERAL: INTERGER_LITERAL ('.' [0-9]*)? ([eE][|-][0-9]+)?;
STRING_LITERAL: '"'('\\'[bfrnt\\"]|~[\n"EOF])*'"';
DECIMAL_LITERAL: ('0' | [1-9] (([0-9] ([0-9_]* [0-9])?)? | '_'+ ([0-9] ([0-9_]* [0-9])?))) [lL]?;
HEX_LITERAL: '0' [xX] [0-9a-fA-F] ([0-9a-fA-F_]* [0-9a-fA-F])? [lL]?;
OCT_LITERAL: '0' '_'* [0-7] ([0-7_]* [0-7])? [lL]?;
BINARY_LITERAL: '0' [bB] [01] ([01_]* [01])? [lL]?;
NULL_LITERAL: KEY_NIL;
CHAR_LITERAL: '\'' (~['\\\r\n] | '\\' [btnfr"'\\] | '\\' ([0-3]? [0-7])? [0-7] | '\\' 'u'+ [0-9a-fA-F] [0-9a-fA-F] [0-9a-fA-F] [0-9a-fA-F]) '\'';
LITERALS: INTERGER_LITERAL | FLOAT_LITERAL | CHAR_LITERAL | STRING_LITERAL | BOOLEAN_LITERAL | NULL_LITERAL;

// Exceptions
ILLEGAL_ESCAPE: '"'('\\'[bfrnt\\"]|~[\n\\"EOF])*('\\'(~[bfrnt\\"]|EOF))('\\'[bfrnt\\"]|~[\n\\"EOF])*'"'?;
UNCLOSE_STRING: '"'('\\'[bfrnt\\"]|~[\n"EOF])*;
UNTERMINATED_COMMENT: '/*'('*'~'/'|~'*')*EOF;
UNTERMINATED_COMMENT_INLINE: '%'(~'%')*EOF;

ERROR_TOKEN: .;
