/**
 * Student name: Nguyen Gia Hung
 * Student ID/Email: hungng89@gmail.com
 */
grammar BKOOL;

@lexer::header{
	package bkool.parser;
}

@lexer::members{
@Override
public Token emit() {
    switch (getType()) {
        case ILLEGAL_ESCAPE:
            Token result = super.emit();
            throw new IllegalEscape(result.getText());

        case UNCLOSE_STRING:
            result = super.emit();
            throw new UncloseString(result.getText());

        case ERROR_TOKEN:
            result = super.emit();
            throw new ErrorToken(result.getText());

        case UNTERMINATED_COMMENT:
            result = super.emit();
            throw new UnterminatedComment();

        case UNTERMINATED_COMMENT_INLINE:
            result = super.emit();
            throw new UnterminatedComment();

        default:
            return super.emit();
        }
    }
}

@parser::header{
	package bkool.parser;
}

options{
	language=Java;
}

program: 'class' ID SPRT_LP SPRT_RP EOF;

// White spaces
WS : [ \t\r\n]+ -> skip;

// Comments
BC: '/*' .*? '*/' -> skip; // Block comment
LC: '%%' ~[\r\n]* -> skip; // Line comment

// Keywords
KEYW_BOOLEAN: 'boolean';
KEYW_EXTENDS: 'extends';
KEYW_FINAL: 'final';
KEYW_CONTINUE: 'continue';
KEYW_INT: 'int';
KEYW_TRUE: 'true';
KEYW_STATIC: 'static';
KEYW_DO: 'do';
KEYW_NEW: 'new';
KEYW_FALSE: 'false';
KEYW_TO: 'to';
KEYW_ELSE: 'else';
KEYW_STRING: 'string';
KEYW_VOID: 'void';
KEYW_DOWNTO: 'downto';
KEYW_THEN: 'then';
KEYW_NIL: 'nil';
KEYW_BREAK: 'break';
KEYW_FLOAT: 'float';
KEYW_FOR: 'for';
KEYW_THIS: 'this';
KEYW_CLASS: 'class';
KEYW_IF: 'if';
KEYW_RETURN: 'return';

// Identifiers
ID: [a-zA-Z_]+[a-zA-Z_0-9]*;

// Literals
INTERGER_LITERAL: '0'|[1-9][0-9]*;
BOOLEAN_LITERAL: KEYW_FALSE | KEYW_TRUE;
FLOAT_LITERAL: INTERGER_LITERAL ('.' [0-9]*)? ([eE][|-][0-9]+)?;

// Separators
SPRT_LP: '{';
SPRT_RP: '}';
SPRT_LRQB: '|';
SPRT_LB: '(';
SPRT_RB: ')';
SPRT_SCOL: ';';
SPRT_COL: ':';
SPRT_DOT: '.';
SPRT_COM: ',';

// Operators
OPRT_NOTEQUAL: '!=';
OPRT_EQUAL: '==';
OPRT_LT: '<';
OPRT_GT: '>';
OPRT_LE: '<=';
OPRT_GE: '>=';
OPRT_OR: '||';
OPRT_AND: '&&';
OPRT_BANG: '!';
OPRT_CARET: '^';
OPRT_ADD: SPRT_LRQB;
OPRT_SUB: '-';
OPRT_MUL: '*';
OPRT_FLOAT_DIV: '/';
OPRT_INT_DIV: '\\';
OPRT_MOD: '%';

// Exceptions
ILLEGAL_ESCAPE: '"'('\\'[bfrnt\\"]|~[\n\\"EOF])*('\\'(~[bfrnt\\"]|EOF))('\\'[bfrnt\\"]|~[\n\\"EOF])*'"'?;
UNCLOSE_STRING: '"'('\\'[bfrnt\\"]|~[\n"EOF])*;
STRING_LITERAL: '"'('\\'[bfrnt\\"]|~[\n"EOF])*'"';
UNTERMINATED_COMMENT: '/*'('*'~'/'|~'*')*EOF;
UNTERMINATED_COMMENT_INLINE: '%'(~'%')*EOF;

ERROR_TOKEN: .;
