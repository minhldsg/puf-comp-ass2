/**
 * Student name: Nguyen Gia Hung
 * Student ID/Email: hungng89@gmail.com
 */
grammar BKOOL;

@lexer::header{
    package bkool.parser;
}

@lexer::members{
@Override
public Token emit() {
    switch (getType()) {
        case ILLEGAL_ESCAPE:
            Token result = super.emit();
            throw new IllegalEscape(result.getText());

        case UNCLOSE_STRING:
            result = super.emit();
            throw new UncloseString(result.getText());

        case ERROR_TOKEN:
            result = super.emit();
            throw new ErrorToken(result.getText());

        case UNTERMINATED_COMMENT:
            result = super.emit();
            throw new UnterminatedComment();

        case UNTERMINATED_COMMENT_INLINE:
            result = super.emit();
            throw new UnterminatedComment();

        default:
            return super.emit();
        }
    }
}

@parser::header{
	package bkool.parser;
}

options{
	language=Java;
}

// Recognizer
program: KEYW_CLASS ID (KEYW_EXTENDS superClass)? classBody;

superClass: ID;

classBody: SPRT_LP memberList SPRT_RP;

memberList: (modifier* memberDeclaration)*;

modifier: KEYW_STATIC | KEYW_FINAL;

memberDeclaration: methodDeclaration | variableDeclaration | constantDeclaration;

constantDeclaration: KEYW_STATIC? KEYW_FINAL type ID OPRT_ASSIGN_CONST expression SPRT_SM;

variableDeclaration: KEYW_STATIC? idList SPRT_COL type SPRT_SM;

methodDeclaration: type KEYW_STATIC? ID SPRT_LB parameterList SPRT_RB blockStatement;

parameterList: (argument (SPRT_COM argument)*)? ;

argument: ID SPRT_COL type;

methodCall: ID SPRT_LB expressionList? SPRT_RB;

blockStatement: SPRT_LP (localVariableDeclaration | statement)* SPRT_RP;

statement: (KEYW_THIS SPRT_DOT)? ID bop=OPRT_ASSIGN_VARIABLE expression SPRT_SM
    | expression SPRT_SM
    | expression SPRT_LQB expression SPRT_RQB OPRT_ASSIGN_VARIABLE expression SPRT_SM
    | KEYW_IF expression KEYW_THEN statement (KEYW_ELSE statement)?
    | KEYW_FOR forVariableDeclaration (KEYW_TO | KEYW_DOWNTO) expression KEYW_DO statement
    | KEYW_BREAK SPRT_SM
    | KEYW_CONTINUE SPRT_SM
    | KEYW_RETURN expression? SPRT_SM
    | ID SPRT_DOT ID parameterList SPRT_SM
    ;

localVariableDeclaration: idList SPRT_COL type SPRT_SM;

type: primitiveType | classType;

primitiveType: KEYW_INT | KEYW_FLOAT | KEYW_BOOLEAN | KEYW_STRING | KEYW_VOID | KEYW_ARRAY;

classType: ID;

idList: ID (SPRT_COM ID)*;

forVariableDeclaration: ID OPRT_ASSIGN_VARIABLE expression;

// Expressions
expression: primary
    | expression bop=SPRT_DOT (ID | methodCall)
    | expression bop=(OPRT_ADD | OPRT_SUB | OPRT_MUL | OPRT_FLOAT_DIV | OPRT_INT_DIV | OPRT_MOD) expression
    | expression bop=(OPRT_AND | OPRT_OR | OPRT_BANG) expression
    | expression (OPRT_EQUAL | OPRT_NOTEQUAL | OPRT_GT | OPRT_LT | OPRT_GE | OPRT_LE) expression
    | expression bop=OPRT_CARET expression
    | expression SPRT_LQB expression SPRT_RQB
    | methodCall
    | <assoc=right> KEYW_NEW classType SPRT_LB expressionList? SPRT_RB
    | <assoc=right> prefix=(OPRT_ADD | OPRT_SUB | OPRT_BANG) expression
    ;

primary: SPRT_LB expression SPRT_RB | KEYW_THIS | INTERGER_LITERAL | FLOAT_LITERAL | CHAR_LITERAL | STRING_LITERAL | BOOLEAN_LITERAL | NULL_LITERAL | ID;

expressionList: expression (SPRT_COM expression)*;

// Lexer
// White spaces
WS : [ \t\r\n]+ -> skip;
// Comments
BC: '/*' .*? '*/' -> skip; // Block comment
LC: '%%' ~[\r\n]* -> skip; // Line comment
// Keywords
KEYW_BOOLEAN: 'boolean';
KEYW_EXTENDS: 'extends';
KEYW_FINAL: 'final';
KEYW_CONTINUE: 'continue';
KEYW_INT: 'int';
KEYW_TRUE: 'true';
KEYW_STATIC: 'static';
KEYW_DO: 'do';
KEYW_NEW: 'new';
KEYW_FALSE: 'false';
KEYW_TO: 'to';
KEYW_ELSE: 'else';
KEYW_STRING: 'string';
KEYW_VOID: 'void';
KEYW_DOWNTO: 'downto';
KEYW_THEN: 'then';
KEYW_NIL: 'nil';
KEYW_BREAK: 'break';
KEYW_FLOAT: 'float';
KEYW_FOR: 'for';
KEYW_THIS: 'this';
KEYW_CLASS: 'class';
KEYW_IF: 'if';
KEYW_RETURN: 'return';
KEYW_ARRAY: 'array';
// Identifiers
ID: [a-zA-Z_]+[a-zA-Z_0-9]*;
// Separators
SPRT_LP: '{';
SPRT_RP: '}';
SPRT_LB: '(';
SPRT_RB: ')';
SPRT_SM: ';';
SPRT_COL: ':';
SPRT_DOT: '.';
SPRT_COM: ',';
SPRT_LQB: '[';
SPRT_RQB: ']';
// Operators
OPRT_ADD: '+';
OPRT_SUB: '-';
OPRT_MUL: '*';
OPRT_INT_DIV: '\\';
OPRT_MOD: '%';
OPRT_FLOAT_DIV: '/';
OPRT_NOTEQUAL: '!=';
OPRT_EQUAL: '==';
OPRT_LT: '<';
OPRT_GT: '>';
OPRT_LE: '<=';
OPRT_GE: '>=';
OPRT_OR: '||';
OPRT_AND: '&&';
OPRT_BANG: '!';
OPRT_CARET: '^';
OPRT_ASSIGN_CONST: '=';
OPRT_ASSIGN_VARIABLE: ':=';
// Literals
fragment EscapeSequence: '\\' [btnfr"'\\] | '\\' ([0-3]? [0-7])? [0-7]| '\\' 'u'+ HexDigit HexDigit HexDigit HexDigit;
fragment HexDigits: HexDigit ((HexDigit | '_')* HexDigit)?;
fragment HexDigit: [0-9a-fA-F];
fragment Digits: [0-9] ([0-9_]* [0-9])?;
fragment LetterOrDigit: Letter | [0-9];
fragment Letter: [a-zA-Z$_] | ~[\u0000-\u007F\uD800-\uDBFF] | [\uD800-\uDBFF] [\uDC00-\uDFFF];
INTERGER_LITERAL: '0'|[1-9][0-9]*;
BOOLEAN_LITERAL: KEYW_FALSE | KEYW_TRUE;
FLOAT_LITERAL: INTERGER_LITERAL ('.' [0-9]*)? ([eE][|-][0-9]+)?;
STRING_LITERAL: '"'('\\'[bfrnt\\"]|~[\n"EOF])*'"';
DECIMAL_LITERAL: ('0' | [1-9] (Digits? | '_'+ Digits)) [lL]?;
HEX_LITERAL: '0' [xX] [0-9a-fA-F] ([0-9a-fA-F_]* [0-9a-fA-F])? [lL]?;
OCT_LITERAL: '0' '_'* [0-7] ([0-7_]* [0-7])? [lL]?;
BINARY_LITERAL: '0' [bB] [01] ([01_]* [01])? [lL]?;
NULL_LITERAL: KEYW_NIL;
CHAR_LITERAL: '\'' (~['\\\r\n] | EscapeSequence) '\'';
LITERALS: INTERGER_LITERAL | FLOAT_LITERAL | CHAR_LITERAL | STRING_LITERAL | BOOLEAN_LITERAL | NULL_LITERAL;

// Exceptions
ILLEGAL_ESCAPE: '"'('\\'[bfrnt\\"]|~[\n\\"EOF])*('\\'(~[bfrnt\\"]|EOF))('\\'[bfrnt\\"]|~[\n\\"EOF])*'"'?;
UNCLOSE_STRING: '"'('\\'[bfrnt\\"]|~[\n"EOF])*;
UNTERMINATED_COMMENT: '/*'('*'~'/'|~'*')*EOF;
UNTERMINATED_COMMENT_INLINE: '%'(~'%')*EOF;

ERROR_TOKEN: .;
