/**
 * Student name: Nghia Dinh
 * Student ID/Email: blackjackptit@gmail.com
 */
grammar BKOOL;

@lexer::header{
    package bkool.parser;
}

@lexer::members{
@Override
public Token emit() {
    switch (getType()) {
        case ILLEGAL_ESCAPE:
            Token result = super.emit();
            throw new IllegalEscape(result.getText());

        case UNCLOSE_STRING:
            result = super.emit();
            throw new UncloseString(result.getText());

        case ERROR_TOKEN:
            result = super.emit();
            throw new ErrorToken(result.getText());

        case UNTERMINATED_COMMENT:
            result = super.emit();
            throw new UnterminatedComment();

        case UNTERMINATED_COMMENT_INLINE:
            result = super.emit();
            throw new UnterminatedComment();

        default:
            return super.emit();
        }
    }
}

@parser::header{
	package bkool.parser;
}

options{
	language=Java;
}

// Program parts
program
    : KW_CLASS ID
      (KW_EXTENDS classType)?
      classBody
    ;

classBody
    : SE_LP classBodyDeclaration SE_RP
    ;

classBodyDeclaration
    : (modifier* memberDeclaration)*
    ;

modifier
    : KW_STATIC
    | KW_FINAL
    ;

memberDeclaration
    : methodDeclaration
    | variableDeclaration
    | constantDeclaration
    ;

idList
    : ID (SE_COM ID)*
    ;

argument
    : ID SE_COL type
    ;

argumentList
    : SE_LB (argument (SE_COM argument)*)? SE_RB
    ;

methodCall
    : ID SE_LB expressionList? SE_RB
    ;

variableDeclaration
    : KW_STATIC? idList SE_COL type SE_SCOL
    ;

localVariableDeclaration
    : idList SE_COL type SE_SCOL
    ;

forVariableDeclaration
    : ID OP_ASSIGN_VARIABLE expression
    ;

constantDeclaration
    : KW_STATIC? KW_FINAL type ID OP_ASSIGN_CONST expression SE_SCOL
    ;

methodDeclaration
    : type KW_STATIC? ID argumentList block
    ;

block
    : SE_LP blockStatement? SE_RP
    ;

blockStatement
    : ( localVariableDeclaration
      | statement
      )*
    ;

statement
    : (KW_THIS SE_DOT)? ID bop=OP_ASSIGN_VARIABLE expression SE_SCOL
    | expression SE_SCOL
    | expression SE_LQB expression SE_RQB OP_ASSIGN_VARIABLE expression SE_SCOL // array statement
    | KW_IF expression KW_THEN statement (KW_ELSE statement)? // If statement
    | KW_FOR forVariableDeclaration (KW_TO | KW_DOWNTO) expression KW_DO statement // For startment
    | KW_BREAK SE_SCOL // Break statement
    | KW_CONTINUE SE_SCOL // Continue statement
    | KW_RETURN expression? SE_SCOL // Return statement
    | ID SE_DOT ID argumentList SE_SCOL // Invoke method statement
    ;

primary
    : SE_LB expression SE_RB
    | KW_THIS
    | INTERGER_LITERAL
    | FLOAT_LITERAL
    | CHAR_LITERAL
    | STRING_LITERAL
    | BOOLEAN_LITERAL
    | NULL_LITERAL
    | ID
    ;

// Expressions
expression
    : primary
    | expression bop=SE_DOT (ID | methodCall) // Member access
    | expression bop=(OP_ADD | OP_SUB | OP_MUL | OP_FLOAT_DIV | OP_INT_DIV | OP_MOD) expression // Int expr
    | expression bop=(OP_AND | OP_OR | OP_BANG) expression // Bool expr
    | expression (OP_EQUAL | OP_NOTEQUAL | OP_GT | OP_LT | OP_GE | OP_LE) expression // Relational expr
    | expression bop=OP_CARET expression // String expr
    | expression SE_LQB expression SE_RQB // Array expr
    | methodCall
    | <assoc=right> KW_NEW classType SE_LB expressionList? SE_RB // New object
    | <assoc=right> prefix=(OP_ADD | OP_SUB | OP_BANG) expression // Unary
    ;

expressionList
    : expression (SE_COM expression)*
    ;

// Types
type
    : primitiveType
    | classType
    ;

primitiveType
    : KW_INT
    | KW_FLOAT
    | KW_BOOLEAN
    | KW_STRING
    | KW_VOID
    | KW_ARRAY
    ;

classType
    : ID
    ;

// White spaces
WS : [ \t\r\n]+ -> skip;

// Keywords
KW_ARRAY: 'array';
KW_BOOLEAN: 'boolean';
KW_EXTENDS: 'extends';
KW_THEN: 'then';
KW_NIL: 'nil';
KW_BREAK: 'break';
KW_FLOAT: 'float';
KW_FOR: 'for';
KW_THIS: 'this';
KW_CLASS: 'class';
KW_IF: 'if';
KW_RETURN: 'return';
KW_FINAL: 'final';
KW_CONTINUE: 'continue';
KW_INT: 'int';
KW_TRUE: 'true';
KW_STATIC: 'static';
KW_DO: 'do';
KW_NEW: 'new';
KW_FALSE: 'false';
KW_TO: 'to';
KW_ELSE: 'else';
KW_STRING: 'string';
KW_VOID: 'void';
KW_DOWNTO: 'downto';

// Identifiers
ID: [a-zA-Z_]+[a-zA-Z_0-9]*;

// Separators
SE_LP: '{';
SE_RP: '}';
SE_LQB: '[';
SE_RQB: ']';
SE_LB: '(';
SE_RB: ')';
SE_SCOL: ';';
SE_COL: ':';
SE_DOT: '.';
SE_COM: ',';

// Operators
OP_ADD: '+';
OP_SUB: '-';
OP_MUL: '*';
OP_FLOAT_DIV: '/';
OP_INT_DIV: '\\';
OP_MOD: '%';
OP_NOTEQUAL: '!=';
OP_EQUAL: '==';
OP_LT: '<';
OP_GT: '>';
OP_LE: '<=';
OP_GE: '>=';
OP_OR: '||';
OP_AND: '&&';
OP_BANG: '!';
OP_CARET: '^';
OP_ASSIGN_CONST: '=';
OP_ASSIGN_VARIABLE: ':=';

// Comments
fragment LBC: '/*';
fragment RBC: '*/';
BC: LBC .*? RBC -> skip; // Block comment

fragment LLC: '%%';
LC: LLC ~[\r\n]* -> skip; // Line comment

// Exceptions
ILLEGAL_ESCAPE: '"'('\\'[bfrnt\\"]|~[\n\\"EOF])*('\\'(~[bfrnt\\"]|EOF))('\\'[bfrnt\\"]|~[\n\\"EOF])*'"'?;
UNCLOSE_STRING: '"'('\\'[bfrnt\\"]|~[\n"EOF])*;
UNTERMINATED_COMMENT: '/*'('*'~'/'|~'*')*EOF;
UNTERMINATED_COMMENT_INLINE: '%'(~'%')*EOF;

// Literals
fragment EscapeSequence
   : '\\' [btnfr"'\\]
   | '\\' ([0-3]? [0-7])? [0-7]
   | '\\' 'u'+ HexDigit HexDigit HexDigit HexDigit
   ;
fragment HexDigits
   : HexDigit ((HexDigit | '_')* HexDigit)?
   ;
fragment HexDigit
   : [0-9a-fA-F]
   ;
fragment Digits
   : [0-9] ([0-9_]* [0-9])?
   ;
fragment LetterOrDigit
   : Letter
   | [0-9]
   ;
fragment Letter
   : [a-zA-Z$_]
   | ~[\u0000-\u007F\uD800-\uDBFF]
   | [\uD800-\uDBFF] [\uDC00-\uDFFF]
   ;

INTERGER_LITERAL: '0'|[1-9][0-9]*;
BOOLEAN_LITERAL: KW_FALSE | KW_TRUE;
FLOAT_LITERAL: INTERGER_LITERAL ('.' [0-9]*)? ([eE][|-][0-9]+)?;
STRING_LITERAL: '"'('\\'[bfrnt\\"]|~[\n"EOF])*'"';
DECIMAL_LITERAL: ('0' | [1-9] (Digits? | '_'+ Digits)) [lL]?;
HEX_LITERAL: '0' [xX] [0-9a-fA-F] ([0-9a-fA-F_]* [0-9a-fA-F])? [lL]?;
OCT_LITERAL: '0' '_'* [0-7] ([0-7_]* [0-7])? [lL]?;
BINARY_LITERAL: '0' [bB] [01] ([01_]* [01])? [lL]?;
NULL_LITERAL: KW_NIL;
CHAR_LITERAL: '\'' (~['\\\r\n] | EscapeSequence) '\'';

LITERALS
    : INTERGER_LITERAL
    | FLOAT_LITERAL
    | CHAR_LITERAL
    | STRING_LITERAL
    | BOOLEAN_LITERAL
    | NULL_LITERAL
    ;

ERROR_TOKEN: .;
