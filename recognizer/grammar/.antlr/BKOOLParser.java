// Generated from d:\4.Master\10.Compiler\puf-comp-ass2\recognizer\grammar\BKOOL.g4 by ANTLR 4.7.1

	package bkool.parser;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BKOOLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		WS=1, BC=2, LC=3, KEY_CLASS=4, KEY_EXTENDS=5, KEY_STATIC=6, KEY_THIS=7, 
		KEY_NEW=8, KEY_FINAL=9, KEY_VOID=10, KEY_BOOLEAN=11, KEY_STRING=12, KEY_INT=13, 
		KEY_FLOAT=14, KEY_FOR=15, KEY_BREAK=16, KEY_IF=17, KEY_ELSE=18, KEY_THEN=19, 
		KEY_CONTINUE=20, KEY_TRUE=21, KEY_FALSE=22, KEY_DO=23, KEY_TO=24, KEY_DOWNTO=25, 
		KEY_RETURN=26, KEY_NIL=27, KEY_ARRAY=28, ID=29, OPT_ADD=30, OPT_SUB=31, 
		OPT_MUL=32, OPT_FLOAT_DIV=33, OPT_INT_DIV=34, OPT_MOD=35, OPT_NOTEQUAL=36, 
		OPT_EQUAL=37, OPT_LT=38, OPT_LE=39, OPT_GT=40, OPT_GE=41, OPT_OR=42, OPT_AND=43, 
		OPT_BANG=44, OPT_CARET=45, OPT_ASSIGN_CONST=46, OPT_ASSIGN_VARIABLE=47, 
		SPT_LP=48, SPT_RP=49, SPT_LB=50, SPT_RB=51, SPT_LQB=52, SPT_RQB=53, SPT_DOT=54, 
		SPT_COM=55, SPT_SM=56, SPT_COL=57, INTERGER_LITERAL=58, BOOLEAN_LITERAL=59, 
		FLOAT_LITERAL=60, STRING_LITERAL=61, DECIMAL_LITERAL=62, HEX_LITERAL=63, 
		OCT_LITERAL=64, BINARY_LITERAL=65, NULL_LITERAL=66, CHAR_LITERAL=67, LITERALS=68, 
		ILLEGAL_ESCAPE=69, UNCLOSE_STRING=70, UNTERMINATED_COMMENT=71, UNTERMINATED_COMMENT_INLINE=72, 
		ERROR_TOKEN=73;
	public static final int
		RULE_program = 0, RULE_super_class = 1, RULE_class_body = 2, RULE_members = 3, 
		RULE_mod = 4, RULE_member_decl = 5, RULE_constant_decl = 6, RULE_variable_decl = 7, 
		RULE_method_decl = 8, RULE_params = 9, RULE_param = 10, RULE_block_stmt = 11, 
		RULE_local_var_decl = 12, RULE_stmt = 13, RULE_for_var_decl = 14, RULE_type = 15, 
		RULE_type_pri = 16, RULE_type_class = 17, RULE_ids = 18, RULE_call_method = 19, 
		RULE_exprs = 20, RULE_expr = 21, RULE_primary = 22;
	public static final String[] ruleNames = {
		"program", "super_class", "class_body", "members", "mod", "member_decl", 
		"constant_decl", "variable_decl", "method_decl", "params", "param", "block_stmt", 
		"local_var_decl", "stmt", "for_var_decl", "type", "type_pri", "type_class", 
		"ids", "call_method", "exprs", "expr", "primary"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, "'class'", "'extends'", "'static'", "'this'", 
		"'new'", "'final'", "'void'", "'boolean'", "'string'", "'int'", "'float'", 
		"'for'", "'break'", "'if'", "'else'", "'then'", "'continue'", "'true'", 
		"'false'", "'do'", "'to'", "'downto'", "'return'", "'nil'", "'array'", 
		null, "'+'", "'-'", "'*'", "'/'", "'\\'", "'%'", "'!='", "'=='", "'<'", 
		"'<='", "'>'", "'>='", "'||'", "'&&'", "'!'", "'^'", "'='", "':='", "'{'", 
		"'}'", "'('", "')'", "'['", "']'", "'.'", "','", "';'", "':'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "WS", "BC", "LC", "KEY_CLASS", "KEY_EXTENDS", "KEY_STATIC", "KEY_THIS", 
		"KEY_NEW", "KEY_FINAL", "KEY_VOID", "KEY_BOOLEAN", "KEY_STRING", "KEY_INT", 
		"KEY_FLOAT", "KEY_FOR", "KEY_BREAK", "KEY_IF", "KEY_ELSE", "KEY_THEN", 
		"KEY_CONTINUE", "KEY_TRUE", "KEY_FALSE", "KEY_DO", "KEY_TO", "KEY_DOWNTO", 
		"KEY_RETURN", "KEY_NIL", "KEY_ARRAY", "ID", "OPT_ADD", "OPT_SUB", "OPT_MUL", 
		"OPT_FLOAT_DIV", "OPT_INT_DIV", "OPT_MOD", "OPT_NOTEQUAL", "OPT_EQUAL", 
		"OPT_LT", "OPT_LE", "OPT_GT", "OPT_GE", "OPT_OR", "OPT_AND", "OPT_BANG", 
		"OPT_CARET", "OPT_ASSIGN_CONST", "OPT_ASSIGN_VARIABLE", "SPT_LP", "SPT_RP", 
		"SPT_LB", "SPT_RB", "SPT_LQB", "SPT_RQB", "SPT_DOT", "SPT_COM", "SPT_SM", 
		"SPT_COL", "INTERGER_LITERAL", "BOOLEAN_LITERAL", "FLOAT_LITERAL", "STRING_LITERAL", 
		"DECIMAL_LITERAL", "HEX_LITERAL", "OCT_LITERAL", "BINARY_LITERAL", "NULL_LITERAL", 
		"CHAR_LITERAL", "LITERALS", "ILLEGAL_ESCAPE", "UNCLOSE_STRING", "UNTERMINATED_COMMENT", 
		"UNTERMINATED_COMMENT_INLINE", "ERROR_TOKEN"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "BKOOL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BKOOLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public TerminalNode KEY_CLASS() { return getToken(BKOOLParser.KEY_CLASS, 0); }
		public TerminalNode ID() { return getToken(BKOOLParser.ID, 0); }
		public Class_bodyContext class_body() {
			return getRuleContext(Class_bodyContext.class,0);
		}
		public TerminalNode KEY_EXTENDS() { return getToken(BKOOLParser.KEY_EXTENDS, 0); }
		public Super_classContext super_class() {
			return getRuleContext(Super_classContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(46);
			match(KEY_CLASS);
			setState(47);
			match(ID);
			setState(50);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KEY_EXTENDS) {
				{
				setState(48);
				match(KEY_EXTENDS);
				setState(49);
				super_class();
				}
			}

			setState(52);
			class_body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Super_classContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(BKOOLParser.ID, 0); }
		public Super_classContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_super_class; }
	}

	public final Super_classContext super_class() throws RecognitionException {
		Super_classContext _localctx = new Super_classContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_super_class);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(54);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_bodyContext extends ParserRuleContext {
		public TerminalNode SPT_LP() { return getToken(BKOOLParser.SPT_LP, 0); }
		public MembersContext members() {
			return getRuleContext(MembersContext.class,0);
		}
		public TerminalNode SPT_RP() { return getToken(BKOOLParser.SPT_RP, 0); }
		public Class_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_body; }
	}

	public final Class_bodyContext class_body() throws RecognitionException {
		Class_bodyContext _localctx = new Class_bodyContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_class_body);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(56);
			match(SPT_LP);
			setState(57);
			members();
			setState(58);
			match(SPT_RP);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MembersContext extends ParserRuleContext {
		public List<Member_declContext> member_decl() {
			return getRuleContexts(Member_declContext.class);
		}
		public Member_declContext member_decl(int i) {
			return getRuleContext(Member_declContext.class,i);
		}
		public List<ModContext> mod() {
			return getRuleContexts(ModContext.class);
		}
		public ModContext mod(int i) {
			return getRuleContext(ModContext.class,i);
		}
		public MembersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_members; }
	}

	public final MembersContext members() throws RecognitionException {
		MembersContext _localctx = new MembersContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_members);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(69);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << KEY_STATIC) | (1L << KEY_FINAL) | (1L << KEY_VOID) | (1L << KEY_BOOLEAN) | (1L << KEY_STRING) | (1L << KEY_INT) | (1L << KEY_FLOAT) | (1L << KEY_ARRAY) | (1L << ID))) != 0)) {
				{
				{
				setState(63);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(60);
						mod();
						}
						} 
					}
					setState(65);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
				}
				setState(66);
				member_decl();
				}
				}
				setState(71);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModContext extends ParserRuleContext {
		public TerminalNode KEY_STATIC() { return getToken(BKOOLParser.KEY_STATIC, 0); }
		public TerminalNode KEY_FINAL() { return getToken(BKOOLParser.KEY_FINAL, 0); }
		public ModContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mod; }
	}

	public final ModContext mod() throws RecognitionException {
		ModContext _localctx = new ModContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_mod);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(72);
			_la = _input.LA(1);
			if ( !(_la==KEY_STATIC || _la==KEY_FINAL) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Member_declContext extends ParserRuleContext {
		public Method_declContext method_decl() {
			return getRuleContext(Method_declContext.class,0);
		}
		public Variable_declContext variable_decl() {
			return getRuleContext(Variable_declContext.class,0);
		}
		public Constant_declContext constant_decl() {
			return getRuleContext(Constant_declContext.class,0);
		}
		public Member_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_member_decl; }
	}

	public final Member_declContext member_decl() throws RecognitionException {
		Member_declContext _localctx = new Member_declContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_member_decl);
		try {
			setState(77);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(74);
				method_decl();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(75);
				variable_decl();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(76);
				constant_decl();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Constant_declContext extends ParserRuleContext {
		public TerminalNode KEY_FINAL() { return getToken(BKOOLParser.KEY_FINAL, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(BKOOLParser.ID, 0); }
		public TerminalNode OPT_ASSIGN_CONST() { return getToken(BKOOLParser.OPT_ASSIGN_CONST, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode SPT_SM() { return getToken(BKOOLParser.SPT_SM, 0); }
		public TerminalNode KEY_STATIC() { return getToken(BKOOLParser.KEY_STATIC, 0); }
		public Constant_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant_decl; }
	}

	public final Constant_declContext constant_decl() throws RecognitionException {
		Constant_declContext _localctx = new Constant_declContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_constant_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(80);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KEY_STATIC) {
				{
				setState(79);
				match(KEY_STATIC);
				}
			}

			setState(82);
			match(KEY_FINAL);
			setState(83);
			type();
			setState(84);
			match(ID);
			setState(85);
			match(OPT_ASSIGN_CONST);
			setState(86);
			expr(0);
			setState(87);
			match(SPT_SM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variable_declContext extends ParserRuleContext {
		public IdsContext ids() {
			return getRuleContext(IdsContext.class,0);
		}
		public TerminalNode SPT_COL() { return getToken(BKOOLParser.SPT_COL, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode SPT_SM() { return getToken(BKOOLParser.SPT_SM, 0); }
		public TerminalNode KEY_STATIC() { return getToken(BKOOLParser.KEY_STATIC, 0); }
		public Variable_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_decl; }
	}

	public final Variable_declContext variable_decl() throws RecognitionException {
		Variable_declContext _localctx = new Variable_declContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_variable_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(90);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KEY_STATIC) {
				{
				setState(89);
				match(KEY_STATIC);
				}
			}

			setState(92);
			ids();
			setState(93);
			match(SPT_COL);
			setState(94);
			type();
			setState(95);
			match(SPT_SM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Method_declContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(BKOOLParser.ID, 0); }
		public TerminalNode SPT_LB() { return getToken(BKOOLParser.SPT_LB, 0); }
		public ParamsContext params() {
			return getRuleContext(ParamsContext.class,0);
		}
		public TerminalNode SPT_RB() { return getToken(BKOOLParser.SPT_RB, 0); }
		public Block_stmtContext block_stmt() {
			return getRuleContext(Block_stmtContext.class,0);
		}
		public TerminalNode KEY_STATIC() { return getToken(BKOOLParser.KEY_STATIC, 0); }
		public Method_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_method_decl; }
	}

	public final Method_declContext method_decl() throws RecognitionException {
		Method_declContext _localctx = new Method_declContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_method_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(97);
			type();
			setState(99);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KEY_STATIC) {
				{
				setState(98);
				match(KEY_STATIC);
				}
			}

			setState(101);
			match(ID);
			setState(102);
			match(SPT_LB);
			setState(103);
			params();
			setState(104);
			match(SPT_RB);
			setState(105);
			block_stmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamsContext extends ParserRuleContext {
		public ParamContext param() {
			return getRuleContext(ParamContext.class,0);
		}
		public TerminalNode SPT_COM() { return getToken(BKOOLParser.SPT_COM, 0); }
		public ParamsContext params() {
			return getRuleContext(ParamsContext.class,0);
		}
		public ParamsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_params; }
	}

	public final ParamsContext params() throws RecognitionException {
		ParamsContext _localctx = new ParamsContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_params);
		try {
			setState(112);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(107);
				param();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(108);
				param();
				setState(109);
				match(SPT_COM);
				setState(110);
				params();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(BKOOLParser.ID, 0); }
		public TerminalNode SPT_COL() { return getToken(BKOOLParser.SPT_COL, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ParamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param; }
	}

	public final ParamContext param() throws RecognitionException {
		ParamContext _localctx = new ParamContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_param);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(114);
			match(ID);
			setState(115);
			match(SPT_COL);
			setState(116);
			type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Block_stmtContext extends ParserRuleContext {
		public TerminalNode SPT_LP() { return getToken(BKOOLParser.SPT_LP, 0); }
		public TerminalNode SPT_RP() { return getToken(BKOOLParser.SPT_RP, 0); }
		public List<Local_var_declContext> local_var_decl() {
			return getRuleContexts(Local_var_declContext.class);
		}
		public Local_var_declContext local_var_decl(int i) {
			return getRuleContext(Local_var_declContext.class,i);
		}
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public Block_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block_stmt; }
	}

	public final Block_stmtContext block_stmt() throws RecognitionException {
		Block_stmtContext _localctx = new Block_stmtContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_block_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118);
			match(SPT_LP);
			setState(123);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 7)) & ~0x3f) == 0 && ((1L << (_la - 7)) & ((1L << (KEY_THIS - 7)) | (1L << (KEY_NEW - 7)) | (1L << (KEY_FOR - 7)) | (1L << (KEY_BREAK - 7)) | (1L << (KEY_IF - 7)) | (1L << (KEY_CONTINUE - 7)) | (1L << (KEY_RETURN - 7)) | (1L << (ID - 7)) | (1L << (OPT_ADD - 7)) | (1L << (OPT_SUB - 7)) | (1L << (OPT_BANG - 7)) | (1L << (SPT_LB - 7)) | (1L << (INTERGER_LITERAL - 7)) | (1L << (BOOLEAN_LITERAL - 7)) | (1L << (FLOAT_LITERAL - 7)) | (1L << (STRING_LITERAL - 7)) | (1L << (NULL_LITERAL - 7)) | (1L << (CHAR_LITERAL - 7)))) != 0)) {
				{
				setState(121);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
				case 1:
					{
					setState(119);
					local_var_decl();
					}
					break;
				case 2:
					{
					setState(120);
					stmt();
					}
					break;
				}
				}
				setState(125);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(126);
			match(SPT_RP);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Local_var_declContext extends ParserRuleContext {
		public IdsContext ids() {
			return getRuleContext(IdsContext.class,0);
		}
		public TerminalNode SPT_COL() { return getToken(BKOOLParser.SPT_COL, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode SPT_SM() { return getToken(BKOOLParser.SPT_SM, 0); }
		public Local_var_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_local_var_decl; }
	}

	public final Local_var_declContext local_var_decl() throws RecognitionException {
		Local_var_declContext _localctx = new Local_var_declContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_local_var_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(128);
			ids();
			setState(129);
			match(SPT_COL);
			setState(130);
			type();
			setState(131);
			match(SPT_SM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StmtContext extends ParserRuleContext {
		public Token bop;
		public List<TerminalNode> ID() { return getTokens(BKOOLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(BKOOLParser.ID, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode SPT_SM() { return getToken(BKOOLParser.SPT_SM, 0); }
		public TerminalNode OPT_ASSIGN_VARIABLE() { return getToken(BKOOLParser.OPT_ASSIGN_VARIABLE, 0); }
		public TerminalNode KEY_THIS() { return getToken(BKOOLParser.KEY_THIS, 0); }
		public TerminalNode SPT_DOT() { return getToken(BKOOLParser.SPT_DOT, 0); }
		public TerminalNode SPT_LQB() { return getToken(BKOOLParser.SPT_LQB, 0); }
		public TerminalNode SPT_RQB() { return getToken(BKOOLParser.SPT_RQB, 0); }
		public TerminalNode KEY_IF() { return getToken(BKOOLParser.KEY_IF, 0); }
		public TerminalNode KEY_THEN() { return getToken(BKOOLParser.KEY_THEN, 0); }
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public TerminalNode KEY_ELSE() { return getToken(BKOOLParser.KEY_ELSE, 0); }
		public TerminalNode KEY_FOR() { return getToken(BKOOLParser.KEY_FOR, 0); }
		public For_var_declContext for_var_decl() {
			return getRuleContext(For_var_declContext.class,0);
		}
		public TerminalNode KEY_DO() { return getToken(BKOOLParser.KEY_DO, 0); }
		public TerminalNode KEY_TO() { return getToken(BKOOLParser.KEY_TO, 0); }
		public TerminalNode KEY_DOWNTO() { return getToken(BKOOLParser.KEY_DOWNTO, 0); }
		public TerminalNode KEY_BREAK() { return getToken(BKOOLParser.KEY_BREAK, 0); }
		public TerminalNode KEY_CONTINUE() { return getToken(BKOOLParser.KEY_CONTINUE, 0); }
		public TerminalNode KEY_RETURN() { return getToken(BKOOLParser.KEY_RETURN, 0); }
		public ParamsContext params() {
			return getRuleContext(ParamsContext.class,0);
		}
		public StmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt; }
	}

	public final StmtContext stmt() throws RecognitionException {
		StmtContext _localctx = new StmtContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_stmt);
		int _la;
		try {
			setState(183);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(135);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==KEY_THIS) {
					{
					setState(133);
					match(KEY_THIS);
					setState(134);
					match(SPT_DOT);
					}
				}

				setState(137);
				match(ID);
				setState(138);
				((StmtContext)_localctx).bop = match(OPT_ASSIGN_VARIABLE);
				setState(139);
				expr(0);
				setState(140);
				match(SPT_SM);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(142);
				expr(0);
				setState(143);
				match(SPT_SM);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(145);
				expr(0);
				setState(146);
				match(SPT_LQB);
				setState(147);
				expr(0);
				setState(148);
				match(SPT_RQB);
				setState(149);
				match(OPT_ASSIGN_VARIABLE);
				setState(150);
				expr(0);
				setState(151);
				match(SPT_SM);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(153);
				match(KEY_IF);
				setState(154);
				expr(0);
				setState(155);
				match(KEY_THEN);
				setState(156);
				stmt();
				setState(159);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
				case 1:
					{
					setState(157);
					match(KEY_ELSE);
					setState(158);
					stmt();
					}
					break;
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(161);
				match(KEY_FOR);
				setState(162);
				for_var_decl();
				setState(163);
				_la = _input.LA(1);
				if ( !(_la==KEY_TO || _la==KEY_DOWNTO) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(164);
				expr(0);
				setState(165);
				match(KEY_DO);
				setState(166);
				stmt();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(168);
				match(KEY_BREAK);
				setState(169);
				match(SPT_SM);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(170);
				match(KEY_CONTINUE);
				setState(171);
				match(SPT_SM);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(172);
				match(KEY_RETURN);
				setState(174);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 7)) & ~0x3f) == 0 && ((1L << (_la - 7)) & ((1L << (KEY_THIS - 7)) | (1L << (KEY_NEW - 7)) | (1L << (ID - 7)) | (1L << (OPT_ADD - 7)) | (1L << (OPT_SUB - 7)) | (1L << (OPT_BANG - 7)) | (1L << (SPT_LB - 7)) | (1L << (INTERGER_LITERAL - 7)) | (1L << (BOOLEAN_LITERAL - 7)) | (1L << (FLOAT_LITERAL - 7)) | (1L << (STRING_LITERAL - 7)) | (1L << (NULL_LITERAL - 7)) | (1L << (CHAR_LITERAL - 7)))) != 0)) {
					{
					setState(173);
					expr(0);
					}
				}

				setState(176);
				match(SPT_SM);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(177);
				match(ID);
				setState(178);
				match(SPT_DOT);
				setState(179);
				match(ID);
				setState(180);
				params();
				setState(181);
				match(SPT_SM);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_var_declContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(BKOOLParser.ID, 0); }
		public TerminalNode OPT_ASSIGN_VARIABLE() { return getToken(BKOOLParser.OPT_ASSIGN_VARIABLE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public For_var_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_var_decl; }
	}

	public final For_var_declContext for_var_decl() throws RecognitionException {
		For_var_declContext _localctx = new For_var_declContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_for_var_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(185);
			match(ID);
			setState(186);
			match(OPT_ASSIGN_VARIABLE);
			setState(187);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public Type_priContext type_pri() {
			return getRuleContext(Type_priContext.class,0);
		}
		public Type_classContext type_class() {
			return getRuleContext(Type_classContext.class,0);
		}
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_type);
		try {
			setState(191);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case KEY_VOID:
			case KEY_BOOLEAN:
			case KEY_STRING:
			case KEY_INT:
			case KEY_FLOAT:
			case KEY_ARRAY:
				enterOuterAlt(_localctx, 1);
				{
				setState(189);
				type_pri();
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(190);
				type_class();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_priContext extends ParserRuleContext {
		public TerminalNode KEY_INT() { return getToken(BKOOLParser.KEY_INT, 0); }
		public TerminalNode KEY_FLOAT() { return getToken(BKOOLParser.KEY_FLOAT, 0); }
		public TerminalNode KEY_BOOLEAN() { return getToken(BKOOLParser.KEY_BOOLEAN, 0); }
		public TerminalNode KEY_STRING() { return getToken(BKOOLParser.KEY_STRING, 0); }
		public TerminalNode KEY_VOID() { return getToken(BKOOLParser.KEY_VOID, 0); }
		public TerminalNode KEY_ARRAY() { return getToken(BKOOLParser.KEY_ARRAY, 0); }
		public Type_priContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_pri; }
	}

	public final Type_priContext type_pri() throws RecognitionException {
		Type_priContext _localctx = new Type_priContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_type_pri);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(193);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << KEY_VOID) | (1L << KEY_BOOLEAN) | (1L << KEY_STRING) | (1L << KEY_INT) | (1L << KEY_FLOAT) | (1L << KEY_ARRAY))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_classContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(BKOOLParser.ID, 0); }
		public Type_classContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_class; }
	}

	public final Type_classContext type_class() throws RecognitionException {
		Type_classContext _localctx = new Type_classContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_type_class);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(195);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdsContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(BKOOLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(BKOOLParser.ID, i);
		}
		public List<TerminalNode> SPT_COM() { return getTokens(BKOOLParser.SPT_COM); }
		public TerminalNode SPT_COM(int i) {
			return getToken(BKOOLParser.SPT_COM, i);
		}
		public IdsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ids; }
	}

	public final IdsContext ids() throws RecognitionException {
		IdsContext _localctx = new IdsContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_ids);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(197);
			match(ID);
			setState(202);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPT_COM) {
				{
				{
				setState(198);
				match(SPT_COM);
				setState(199);
				match(ID);
				}
				}
				setState(204);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Call_methodContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(BKOOLParser.ID, 0); }
		public TerminalNode SPT_LB() { return getToken(BKOOLParser.SPT_LB, 0); }
		public TerminalNode SPT_RB() { return getToken(BKOOLParser.SPT_RB, 0); }
		public ExprsContext exprs() {
			return getRuleContext(ExprsContext.class,0);
		}
		public Call_methodContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_call_method; }
	}

	public final Call_methodContext call_method() throws RecognitionException {
		Call_methodContext _localctx = new Call_methodContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_call_method);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(205);
			match(ID);
			setState(206);
			match(SPT_LB);
			setState(208);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 7)) & ~0x3f) == 0 && ((1L << (_la - 7)) & ((1L << (KEY_THIS - 7)) | (1L << (KEY_NEW - 7)) | (1L << (ID - 7)) | (1L << (OPT_ADD - 7)) | (1L << (OPT_SUB - 7)) | (1L << (OPT_BANG - 7)) | (1L << (SPT_LB - 7)) | (1L << (INTERGER_LITERAL - 7)) | (1L << (BOOLEAN_LITERAL - 7)) | (1L << (FLOAT_LITERAL - 7)) | (1L << (STRING_LITERAL - 7)) | (1L << (NULL_LITERAL - 7)) | (1L << (CHAR_LITERAL - 7)))) != 0)) {
				{
				setState(207);
				exprs();
				}
			}

			setState(210);
			match(SPT_RB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprsContext extends ParserRuleContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> SPT_COM() { return getTokens(BKOOLParser.SPT_COM); }
		public TerminalNode SPT_COM(int i) {
			return getToken(BKOOLParser.SPT_COM, i);
		}
		public ExprsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exprs; }
	}

	public final ExprsContext exprs() throws RecognitionException {
		ExprsContext _localctx = new ExprsContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_exprs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(212);
			expr(0);
			setState(217);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPT_COM) {
				{
				{
				setState(213);
				match(SPT_COM);
				setState(214);
				expr(0);
				}
				}
				setState(219);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public Token prefix;
		public Token bop;
		public PrimaryContext primary() {
			return getRuleContext(PrimaryContext.class,0);
		}
		public Call_methodContext call_method() {
			return getRuleContext(Call_methodContext.class,0);
		}
		public TerminalNode KEY_NEW() { return getToken(BKOOLParser.KEY_NEW, 0); }
		public Type_classContext type_class() {
			return getRuleContext(Type_classContext.class,0);
		}
		public TerminalNode SPT_LB() { return getToken(BKOOLParser.SPT_LB, 0); }
		public TerminalNode SPT_RB() { return getToken(BKOOLParser.SPT_RB, 0); }
		public ExprsContext exprs() {
			return getRuleContext(ExprsContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode OPT_ADD() { return getToken(BKOOLParser.OPT_ADD, 0); }
		public TerminalNode OPT_SUB() { return getToken(BKOOLParser.OPT_SUB, 0); }
		public TerminalNode OPT_BANG() { return getToken(BKOOLParser.OPT_BANG, 0); }
		public TerminalNode OPT_MUL() { return getToken(BKOOLParser.OPT_MUL, 0); }
		public TerminalNode OPT_FLOAT_DIV() { return getToken(BKOOLParser.OPT_FLOAT_DIV, 0); }
		public TerminalNode OPT_INT_DIV() { return getToken(BKOOLParser.OPT_INT_DIV, 0); }
		public TerminalNode OPT_MOD() { return getToken(BKOOLParser.OPT_MOD, 0); }
		public TerminalNode OPT_AND() { return getToken(BKOOLParser.OPT_AND, 0); }
		public TerminalNode OPT_OR() { return getToken(BKOOLParser.OPT_OR, 0); }
		public TerminalNode OPT_EQUAL() { return getToken(BKOOLParser.OPT_EQUAL, 0); }
		public TerminalNode OPT_NOTEQUAL() { return getToken(BKOOLParser.OPT_NOTEQUAL, 0); }
		public TerminalNode OPT_GT() { return getToken(BKOOLParser.OPT_GT, 0); }
		public TerminalNode OPT_LT() { return getToken(BKOOLParser.OPT_LT, 0); }
		public TerminalNode OPT_GE() { return getToken(BKOOLParser.OPT_GE, 0); }
		public TerminalNode OPT_LE() { return getToken(BKOOLParser.OPT_LE, 0); }
		public TerminalNode OPT_CARET() { return getToken(BKOOLParser.OPT_CARET, 0); }
		public TerminalNode SPT_DOT() { return getToken(BKOOLParser.SPT_DOT, 0); }
		public TerminalNode ID() { return getToken(BKOOLParser.ID, 0); }
		public TerminalNode SPT_LQB() { return getToken(BKOOLParser.SPT_LQB, 0); }
		public TerminalNode SPT_RQB() { return getToken(BKOOLParser.SPT_RQB, 0); }
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 42;
		enterRecursionRule(_localctx, 42, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(233);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				setState(221);
				primary();
				}
				break;
			case 2:
				{
				setState(222);
				call_method();
				}
				break;
			case 3:
				{
				setState(223);
				match(KEY_NEW);
				setState(224);
				type_class();
				setState(225);
				match(SPT_LB);
				setState(227);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 7)) & ~0x3f) == 0 && ((1L << (_la - 7)) & ((1L << (KEY_THIS - 7)) | (1L << (KEY_NEW - 7)) | (1L << (ID - 7)) | (1L << (OPT_ADD - 7)) | (1L << (OPT_SUB - 7)) | (1L << (OPT_BANG - 7)) | (1L << (SPT_LB - 7)) | (1L << (INTERGER_LITERAL - 7)) | (1L << (BOOLEAN_LITERAL - 7)) | (1L << (FLOAT_LITERAL - 7)) | (1L << (STRING_LITERAL - 7)) | (1L << (NULL_LITERAL - 7)) | (1L << (CHAR_LITERAL - 7)))) != 0)) {
					{
					setState(226);
					exprs();
					}
				}

				setState(229);
				match(SPT_RB);
				}
				break;
			case 4:
				{
				setState(231);
				((ExprContext)_localctx).prefix = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPT_ADD) | (1L << OPT_SUB) | (1L << OPT_BANG))) != 0)) ) {
					((ExprContext)_localctx).prefix = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(232);
				expr(1);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(260);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(258);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
					case 1:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(235);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(236);
						((ExprContext)_localctx).bop = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPT_ADD) | (1L << OPT_SUB) | (1L << OPT_MUL) | (1L << OPT_FLOAT_DIV) | (1L << OPT_INT_DIV) | (1L << OPT_MOD))) != 0)) ) {
							((ExprContext)_localctx).bop = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(237);
						expr(9);
						}
						break;
					case 2:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(238);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(239);
						((ExprContext)_localctx).bop = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPT_OR) | (1L << OPT_AND) | (1L << OPT_BANG))) != 0)) ) {
							((ExprContext)_localctx).bop = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(240);
						expr(8);
						}
						break;
					case 3:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(241);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(242);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPT_NOTEQUAL) | (1L << OPT_EQUAL) | (1L << OPT_LT) | (1L << OPT_LE) | (1L << OPT_GT) | (1L << OPT_GE))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(243);
						expr(7);
						}
						break;
					case 4:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(244);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(245);
						((ExprContext)_localctx).bop = match(OPT_CARET);
						setState(246);
						expr(6);
						}
						break;
					case 5:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(247);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(248);
						((ExprContext)_localctx).bop = match(SPT_DOT);
						setState(251);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
						case 1:
							{
							setState(249);
							match(ID);
							}
							break;
						case 2:
							{
							setState(250);
							call_method();
							}
							break;
						}
						}
						break;
					case 6:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(253);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(254);
						match(SPT_LQB);
						setState(255);
						expr(0);
						setState(256);
						match(SPT_RQB);
						}
						break;
					}
					} 
				}
				setState(262);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class PrimaryContext extends ParserRuleContext {
		public TerminalNode SPT_LB() { return getToken(BKOOLParser.SPT_LB, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode SPT_RB() { return getToken(BKOOLParser.SPT_RB, 0); }
		public TerminalNode KEY_THIS() { return getToken(BKOOLParser.KEY_THIS, 0); }
		public TerminalNode INTERGER_LITERAL() { return getToken(BKOOLParser.INTERGER_LITERAL, 0); }
		public TerminalNode FLOAT_LITERAL() { return getToken(BKOOLParser.FLOAT_LITERAL, 0); }
		public TerminalNode CHAR_LITERAL() { return getToken(BKOOLParser.CHAR_LITERAL, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(BKOOLParser.STRING_LITERAL, 0); }
		public TerminalNode BOOLEAN_LITERAL() { return getToken(BKOOLParser.BOOLEAN_LITERAL, 0); }
		public TerminalNode NULL_LITERAL() { return getToken(BKOOLParser.NULL_LITERAL, 0); }
		public TerminalNode ID() { return getToken(BKOOLParser.ID, 0); }
		public PrimaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary; }
	}

	public final PrimaryContext primary() throws RecognitionException {
		PrimaryContext _localctx = new PrimaryContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_primary);
		try {
			setState(275);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case SPT_LB:
				enterOuterAlt(_localctx, 1);
				{
				setState(263);
				match(SPT_LB);
				setState(264);
				expr(0);
				setState(265);
				match(SPT_RB);
				}
				break;
			case KEY_THIS:
				enterOuterAlt(_localctx, 2);
				{
				setState(267);
				match(KEY_THIS);
				}
				break;
			case INTERGER_LITERAL:
				enterOuterAlt(_localctx, 3);
				{
				setState(268);
				match(INTERGER_LITERAL);
				}
				break;
			case FLOAT_LITERAL:
				enterOuterAlt(_localctx, 4);
				{
				setState(269);
				match(FLOAT_LITERAL);
				}
				break;
			case CHAR_LITERAL:
				enterOuterAlt(_localctx, 5);
				{
				setState(270);
				match(CHAR_LITERAL);
				}
				break;
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 6);
				{
				setState(271);
				match(STRING_LITERAL);
				}
				break;
			case BOOLEAN_LITERAL:
				enterOuterAlt(_localctx, 7);
				{
				setState(272);
				match(BOOLEAN_LITERAL);
				}
				break;
			case NULL_LITERAL:
				enterOuterAlt(_localctx, 8);
				{
				setState(273);
				match(NULL_LITERAL);
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 9);
				{
				setState(274);
				match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 21:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 8);
		case 1:
			return precpred(_ctx, 7);
		case 2:
			return precpred(_ctx, 6);
		case 3:
			return precpred(_ctx, 5);
		case 4:
			return precpred(_ctx, 9);
		case 5:
			return precpred(_ctx, 4);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3K\u0118\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\3\2\3\2\3"+
		"\2\3\2\5\2\65\n\2\3\2\3\2\3\3\3\3\3\4\3\4\3\4\3\4\3\5\7\5@\n\5\f\5\16"+
		"\5C\13\5\3\5\7\5F\n\5\f\5\16\5I\13\5\3\6\3\6\3\7\3\7\3\7\5\7P\n\7\3\b"+
		"\5\bS\n\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\t\5\t]\n\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\n\3\n\5\nf\n\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\5\13"+
		"s\n\13\3\f\3\f\3\f\3\f\3\r\3\r\3\r\7\r|\n\r\f\r\16\r\177\13\r\3\r\3\r"+
		"\3\16\3\16\3\16\3\16\3\16\3\17\3\17\5\17\u008a\n\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\5\17\u00a2\n\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u00b1\n\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\5\17\u00ba\n\17\3\20\3\20\3\20\3\20\3\21\3\21\5\21\u00c2\n"+
		"\21\3\22\3\22\3\23\3\23\3\24\3\24\3\24\7\24\u00cb\n\24\f\24\16\24\u00ce"+
		"\13\24\3\25\3\25\3\25\5\25\u00d3\n\25\3\25\3\25\3\26\3\26\3\26\7\26\u00da"+
		"\n\26\f\26\16\26\u00dd\13\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\5\27\u00e6"+
		"\n\27\3\27\3\27\3\27\3\27\5\27\u00ec\n\27\3\27\3\27\3\27\3\27\3\27\3\27"+
		"\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\5\27\u00fe\n\27\3\27"+
		"\3\27\3\27\3\27\3\27\7\27\u0105\n\27\f\27\16\27\u0108\13\27\3\30\3\30"+
		"\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\5\30\u0116\n\30\3\30"+
		"\2\3,\31\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\2\t\4\2\b\b\13"+
		"\13\3\2\32\33\4\2\f\20\36\36\4\2 !..\3\2 %\3\2,.\3\2&+\2\u012d\2\60\3"+
		"\2\2\2\48\3\2\2\2\6:\3\2\2\2\bG\3\2\2\2\nJ\3\2\2\2\fO\3\2\2\2\16R\3\2"+
		"\2\2\20\\\3\2\2\2\22c\3\2\2\2\24r\3\2\2\2\26t\3\2\2\2\30x\3\2\2\2\32\u0082"+
		"\3\2\2\2\34\u00b9\3\2\2\2\36\u00bb\3\2\2\2 \u00c1\3\2\2\2\"\u00c3\3\2"+
		"\2\2$\u00c5\3\2\2\2&\u00c7\3\2\2\2(\u00cf\3\2\2\2*\u00d6\3\2\2\2,\u00eb"+
		"\3\2\2\2.\u0115\3\2\2\2\60\61\7\6\2\2\61\64\7\37\2\2\62\63\7\7\2\2\63"+
		"\65\5\4\3\2\64\62\3\2\2\2\64\65\3\2\2\2\65\66\3\2\2\2\66\67\5\6\4\2\67"+
		"\3\3\2\2\289\7\37\2\29\5\3\2\2\2:;\7\62\2\2;<\5\b\5\2<=\7\63\2\2=\7\3"+
		"\2\2\2>@\5\n\6\2?>\3\2\2\2@C\3\2\2\2A?\3\2\2\2AB\3\2\2\2BD\3\2\2\2CA\3"+
		"\2\2\2DF\5\f\7\2EA\3\2\2\2FI\3\2\2\2GE\3\2\2\2GH\3\2\2\2H\t\3\2\2\2IG"+
		"\3\2\2\2JK\t\2\2\2K\13\3\2\2\2LP\5\22\n\2MP\5\20\t\2NP\5\16\b\2OL\3\2"+
		"\2\2OM\3\2\2\2ON\3\2\2\2P\r\3\2\2\2QS\7\b\2\2RQ\3\2\2\2RS\3\2\2\2ST\3"+
		"\2\2\2TU\7\13\2\2UV\5 \21\2VW\7\37\2\2WX\7\60\2\2XY\5,\27\2YZ\7:\2\2Z"+
		"\17\3\2\2\2[]\7\b\2\2\\[\3\2\2\2\\]\3\2\2\2]^\3\2\2\2^_\5&\24\2_`\7;\2"+
		"\2`a\5 \21\2ab\7:\2\2b\21\3\2\2\2ce\5 \21\2df\7\b\2\2ed\3\2\2\2ef\3\2"+
		"\2\2fg\3\2\2\2gh\7\37\2\2hi\7\64\2\2ij\5\24\13\2jk\7\65\2\2kl\5\30\r\2"+
		"l\23\3\2\2\2ms\5\26\f\2no\5\26\f\2op\79\2\2pq\5\24\13\2qs\3\2\2\2rm\3"+
		"\2\2\2rn\3\2\2\2s\25\3\2\2\2tu\7\37\2\2uv\7;\2\2vw\5 \21\2w\27\3\2\2\2"+
		"x}\7\62\2\2y|\5\32\16\2z|\5\34\17\2{y\3\2\2\2{z\3\2\2\2|\177\3\2\2\2}"+
		"{\3\2\2\2}~\3\2\2\2~\u0080\3\2\2\2\177}\3\2\2\2\u0080\u0081\7\63\2\2\u0081"+
		"\31\3\2\2\2\u0082\u0083\5&\24\2\u0083\u0084\7;\2\2\u0084\u0085\5 \21\2"+
		"\u0085\u0086\7:\2\2\u0086\33\3\2\2\2\u0087\u0088\7\t\2\2\u0088\u008a\7"+
		"8\2\2\u0089\u0087\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u008b\3\2\2\2\u008b"+
		"\u008c\7\37\2\2\u008c\u008d\7\61\2\2\u008d\u008e\5,\27\2\u008e\u008f\7"+
		":\2\2\u008f\u00ba\3\2\2\2\u0090\u0091\5,\27\2\u0091\u0092\7:\2\2\u0092"+
		"\u00ba\3\2\2\2\u0093\u0094\5,\27\2\u0094\u0095\7\66\2\2\u0095\u0096\5"+
		",\27\2\u0096\u0097\7\67\2\2\u0097\u0098\7\61\2\2\u0098\u0099\5,\27\2\u0099"+
		"\u009a\7:\2\2\u009a\u00ba\3\2\2\2\u009b\u009c\7\23\2\2\u009c\u009d\5,"+
		"\27\2\u009d\u009e\7\25\2\2\u009e\u00a1\5\34\17\2\u009f\u00a0\7\24\2\2"+
		"\u00a0\u00a2\5\34\17\2\u00a1\u009f\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2\u00ba"+
		"\3\2\2\2\u00a3\u00a4\7\21\2\2\u00a4\u00a5\5\36\20\2\u00a5\u00a6\t\3\2"+
		"\2\u00a6\u00a7\5,\27\2\u00a7\u00a8\7\31\2\2\u00a8\u00a9\5\34\17\2\u00a9"+
		"\u00ba\3\2\2\2\u00aa\u00ab\7\22\2\2\u00ab\u00ba\7:\2\2\u00ac\u00ad\7\26"+
		"\2\2\u00ad\u00ba\7:\2\2\u00ae\u00b0\7\34\2\2\u00af\u00b1\5,\27\2\u00b0"+
		"\u00af\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b2\u00ba\7:"+
		"\2\2\u00b3\u00b4\7\37\2\2\u00b4\u00b5\78\2\2\u00b5\u00b6\7\37\2\2\u00b6"+
		"\u00b7\5\24\13\2\u00b7\u00b8\7:\2\2\u00b8\u00ba\3\2\2\2\u00b9\u0089\3"+
		"\2\2\2\u00b9\u0090\3\2\2\2\u00b9\u0093\3\2\2\2\u00b9\u009b\3\2\2\2\u00b9"+
		"\u00a3\3\2\2\2\u00b9\u00aa\3\2\2\2\u00b9\u00ac\3\2\2\2\u00b9\u00ae\3\2"+
		"\2\2\u00b9\u00b3\3\2\2\2\u00ba\35\3\2\2\2\u00bb\u00bc\7\37\2\2\u00bc\u00bd"+
		"\7\61\2\2\u00bd\u00be\5,\27\2\u00be\37\3\2\2\2\u00bf\u00c2\5\"\22\2\u00c0"+
		"\u00c2\5$\23\2\u00c1\u00bf\3\2\2\2\u00c1\u00c0\3\2\2\2\u00c2!\3\2\2\2"+
		"\u00c3\u00c4\t\4\2\2\u00c4#\3\2\2\2\u00c5\u00c6\7\37\2\2\u00c6%\3\2\2"+
		"\2\u00c7\u00cc\7\37\2\2\u00c8\u00c9\79\2\2\u00c9\u00cb\7\37\2\2\u00ca"+
		"\u00c8\3\2\2\2\u00cb\u00ce\3\2\2\2\u00cc\u00ca\3\2\2\2\u00cc\u00cd\3\2"+
		"\2\2\u00cd\'\3\2\2\2\u00ce\u00cc\3\2\2\2\u00cf\u00d0\7\37\2\2\u00d0\u00d2"+
		"\7\64\2\2\u00d1\u00d3\5*\26\2\u00d2\u00d1\3\2\2\2\u00d2\u00d3\3\2\2\2"+
		"\u00d3\u00d4\3\2\2\2\u00d4\u00d5\7\65\2\2\u00d5)\3\2\2\2\u00d6\u00db\5"+
		",\27\2\u00d7\u00d8\79\2\2\u00d8\u00da\5,\27\2\u00d9\u00d7\3\2\2\2\u00da"+
		"\u00dd\3\2\2\2\u00db\u00d9\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc+\3\2\2\2"+
		"\u00dd\u00db\3\2\2\2\u00de\u00df\b\27\1\2\u00df\u00ec\5.\30\2\u00e0\u00ec"+
		"\5(\25\2\u00e1\u00e2\7\n\2\2\u00e2\u00e3\5$\23\2\u00e3\u00e5\7\64\2\2"+
		"\u00e4\u00e6\5*\26\2\u00e5\u00e4\3\2\2\2\u00e5\u00e6\3\2\2\2\u00e6\u00e7"+
		"\3\2\2\2\u00e7\u00e8\7\65\2\2\u00e8\u00ec\3\2\2\2\u00e9\u00ea\t\5\2\2"+
		"\u00ea\u00ec\5,\27\3\u00eb\u00de\3\2\2\2\u00eb\u00e0\3\2\2\2\u00eb\u00e1"+
		"\3\2\2\2\u00eb\u00e9\3\2\2\2\u00ec\u0106\3\2\2\2\u00ed\u00ee\f\n\2\2\u00ee"+
		"\u00ef\t\6\2\2\u00ef\u0105\5,\27\13\u00f0\u00f1\f\t\2\2\u00f1\u00f2\t"+
		"\7\2\2\u00f2\u0105\5,\27\n\u00f3\u00f4\f\b\2\2\u00f4\u00f5\t\b\2\2\u00f5"+
		"\u0105\5,\27\t\u00f6\u00f7\f\7\2\2\u00f7\u00f8\7/\2\2\u00f8\u0105\5,\27"+
		"\b\u00f9\u00fa\f\13\2\2\u00fa\u00fd\78\2\2\u00fb\u00fe\7\37\2\2\u00fc"+
		"\u00fe\5(\25\2\u00fd\u00fb\3\2\2\2\u00fd\u00fc\3\2\2\2\u00fe\u0105\3\2"+
		"\2\2\u00ff\u0100\f\6\2\2\u0100\u0101\7\66\2\2\u0101\u0102\5,\27\2\u0102"+
		"\u0103\7\67\2\2\u0103\u0105\3\2\2\2\u0104\u00ed\3\2\2\2\u0104\u00f0\3"+
		"\2\2\2\u0104\u00f3\3\2\2\2\u0104\u00f6\3\2\2\2\u0104\u00f9\3\2\2\2\u0104"+
		"\u00ff\3\2\2\2\u0105\u0108\3\2\2\2\u0106\u0104\3\2\2\2\u0106\u0107\3\2"+
		"\2\2\u0107-\3\2\2\2\u0108\u0106\3\2\2\2\u0109\u010a\7\64\2\2\u010a\u010b"+
		"\5,\27\2\u010b\u010c\7\65\2\2\u010c\u0116\3\2\2\2\u010d\u0116\7\t\2\2"+
		"\u010e\u0116\7<\2\2\u010f\u0116\7>\2\2\u0110\u0116\7E\2\2\u0111\u0116"+
		"\7?\2\2\u0112\u0116\7=\2\2\u0113\u0116\7D\2\2\u0114\u0116\7\37\2\2\u0115"+
		"\u0109\3\2\2\2\u0115\u010d\3\2\2\2\u0115\u010e\3\2\2\2\u0115\u010f\3\2"+
		"\2\2\u0115\u0110\3\2\2\2\u0115\u0111\3\2\2\2\u0115\u0112\3\2\2\2\u0115"+
		"\u0113\3\2\2\2\u0115\u0114\3\2\2\2\u0116/\3\2\2\2\32\64AGOR\\er{}\u0089"+
		"\u00a1\u00b0\u00b9\u00c1\u00cc\u00d2\u00db\u00e5\u00eb\u00fd\u0104\u0106"+
		"\u0115";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}