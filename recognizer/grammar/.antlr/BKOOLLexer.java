// Generated from d:\4.Master\10.Compiler\puf-comp-ass2\recognizer\grammar\BKOOL.g4 by ANTLR 4.7.1

    package bkool.parser;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BKOOLLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		WS=1, BC=2, LC=3, KEY_CLASS=4, KEY_EXTENDS=5, KEY_STATIC=6, KEY_THIS=7, 
		KEY_NEW=8, KEY_FINAL=9, KEY_VOID=10, KEY_BOOLEAN=11, KEY_STRING=12, KEY_INT=13, 
		KEY_FLOAT=14, KEY_FOR=15, KEY_BREAK=16, KEY_IF=17, KEY_ELSE=18, KEY_THEN=19, 
		KEY_CONTINUE=20, KEY_TRUE=21, KEY_FALSE=22, KEY_DO=23, KEY_TO=24, KEY_DOWNTO=25, 
		KEY_RETURN=26, KEY_NIL=27, KEY_ARRAY=28, ID=29, OPT_ADD=30, OPT_SUB=31, 
		OPT_MUL=32, OPT_FLOAT_DIV=33, OPT_INT_DIV=34, OPT_MOD=35, OPT_NOTEQUAL=36, 
		OPT_EQUAL=37, OPT_LT=38, OPT_LE=39, OPT_GT=40, OPT_GE=41, OPT_OR=42, OPT_AND=43, 
		OPT_BANG=44, OPT_CARET=45, OPT_ASSIGN_CONST=46, OPT_ASSIGN_VARIABLE=47, 
		SPT_LP=48, SPT_RP=49, SPT_LB=50, SPT_RB=51, SPT_LQB=52, SPT_RQB=53, SPT_DOT=54, 
		SPT_COM=55, SPT_SM=56, SPT_COL=57, INTERGER_LITERAL=58, BOOLEAN_LITERAL=59, 
		FLOAT_LITERAL=60, STRING_LITERAL=61, DECIMAL_LITERAL=62, HEX_LITERAL=63, 
		OCT_LITERAL=64, BINARY_LITERAL=65, NULL_LITERAL=66, CHAR_LITERAL=67, LITERALS=68, 
		ILLEGAL_ESCAPE=69, UNCLOSE_STRING=70, UNTERMINATED_COMMENT=71, UNTERMINATED_COMMENT_INLINE=72, 
		ERROR_TOKEN=73;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"WS", "BC", "LC", "KEY_CLASS", "KEY_EXTENDS", "KEY_STATIC", "KEY_THIS", 
		"KEY_NEW", "KEY_FINAL", "KEY_VOID", "KEY_BOOLEAN", "KEY_STRING", "KEY_INT", 
		"KEY_FLOAT", "KEY_FOR", "KEY_BREAK", "KEY_IF", "KEY_ELSE", "KEY_THEN", 
		"KEY_CONTINUE", "KEY_TRUE", "KEY_FALSE", "KEY_DO", "KEY_TO", "KEY_DOWNTO", 
		"KEY_RETURN", "KEY_NIL", "KEY_ARRAY", "ID", "OPT_ADD", "OPT_SUB", "OPT_MUL", 
		"OPT_FLOAT_DIV", "OPT_INT_DIV", "OPT_MOD", "OPT_NOTEQUAL", "OPT_EQUAL", 
		"OPT_LT", "OPT_LE", "OPT_GT", "OPT_GE", "OPT_OR", "OPT_AND", "OPT_BANG", 
		"OPT_CARET", "OPT_ASSIGN_CONST", "OPT_ASSIGN_VARIABLE", "SPT_LP", "SPT_RP", 
		"SPT_LB", "SPT_RB", "SPT_LQB", "SPT_RQB", "SPT_DOT", "SPT_COM", "SPT_SM", 
		"SPT_COL", "INTERGER_LITERAL", "BOOLEAN_LITERAL", "FLOAT_LITERAL", "STRING_LITERAL", 
		"DECIMAL_LITERAL", "HEX_LITERAL", "OCT_LITERAL", "BINARY_LITERAL", "NULL_LITERAL", 
		"CHAR_LITERAL", "LITERALS", "ILLEGAL_ESCAPE", "UNCLOSE_STRING", "UNTERMINATED_COMMENT", 
		"UNTERMINATED_COMMENT_INLINE", "ERROR_TOKEN"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, "'class'", "'extends'", "'static'", "'this'", 
		"'new'", "'final'", "'void'", "'boolean'", "'string'", "'int'", "'float'", 
		"'for'", "'break'", "'if'", "'else'", "'then'", "'continue'", "'true'", 
		"'false'", "'do'", "'to'", "'downto'", "'return'", "'nil'", "'array'", 
		null, "'+'", "'-'", "'*'", "'/'", "'\\'", "'%'", "'!='", "'=='", "'<'", 
		"'<='", "'>'", "'>='", "'||'", "'&&'", "'!'", "'^'", "'='", "':='", "'{'", 
		"'}'", "'('", "')'", "'['", "']'", "'.'", "','", "';'", "':'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "WS", "BC", "LC", "KEY_CLASS", "KEY_EXTENDS", "KEY_STATIC", "KEY_THIS", 
		"KEY_NEW", "KEY_FINAL", "KEY_VOID", "KEY_BOOLEAN", "KEY_STRING", "KEY_INT", 
		"KEY_FLOAT", "KEY_FOR", "KEY_BREAK", "KEY_IF", "KEY_ELSE", "KEY_THEN", 
		"KEY_CONTINUE", "KEY_TRUE", "KEY_FALSE", "KEY_DO", "KEY_TO", "KEY_DOWNTO", 
		"KEY_RETURN", "KEY_NIL", "KEY_ARRAY", "ID", "OPT_ADD", "OPT_SUB", "OPT_MUL", 
		"OPT_FLOAT_DIV", "OPT_INT_DIV", "OPT_MOD", "OPT_NOTEQUAL", "OPT_EQUAL", 
		"OPT_LT", "OPT_LE", "OPT_GT", "OPT_GE", "OPT_OR", "OPT_AND", "OPT_BANG", 
		"OPT_CARET", "OPT_ASSIGN_CONST", "OPT_ASSIGN_VARIABLE", "SPT_LP", "SPT_RP", 
		"SPT_LB", "SPT_RB", "SPT_LQB", "SPT_RQB", "SPT_DOT", "SPT_COM", "SPT_SM", 
		"SPT_COL", "INTERGER_LITERAL", "BOOLEAN_LITERAL", "FLOAT_LITERAL", "STRING_LITERAL", 
		"DECIMAL_LITERAL", "HEX_LITERAL", "OCT_LITERAL", "BINARY_LITERAL", "NULL_LITERAL", 
		"CHAR_LITERAL", "LITERALS", "ILLEGAL_ESCAPE", "UNCLOSE_STRING", "UNTERMINATED_COMMENT", 
		"UNTERMINATED_COMMENT_INLINE", "ERROR_TOKEN"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	@Override
	public Token emit() {
	    switch (getType()) {
	        case ILLEGAL_ESCAPE:
	            Token result = super.emit();
	            throw new IllegalEscape(result.getText());

	        case UNCLOSE_STRING:
	            result = super.emit();
	            throw new UncloseString(result.getText());

	        case ERROR_TOKEN:
	            result = super.emit();
	            throw new ErrorToken(result.getText());

	        case UNTERMINATED_COMMENT:
	            result = super.emit();
	            throw new UnterminatedComment();

	        case UNTERMINATED_COMMENT_INLINE:
	            result = super.emit();
	            throw new UnterminatedComment();

	        default:
	            return super.emit();
	        }
	    }


	public BKOOLLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "BKOOL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2K\u026a\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\3\2\6\2\u0097\n\2\r\2\16\2\u0098\3\2\3\2\3\3\3\3\3\3\3\3\7\3"+
		"\u00a1\n\3\f\3\16\3\u00a4\13\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\7\4"+
		"\u00af\n\4\f\4\16\4\u00b2\13\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3"+
		"\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3"+
		"\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\21\3\21\3"+
		"\21\3\21\3\21\3\21\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3"+
		"\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3"+
		"\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\31\3\31\3"+
		"\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\33\3"+
		"\33\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\35\3\36\6\36\u0142"+
		"\n\36\r\36\16\36\u0143\3\36\7\36\u0147\n\36\f\36\16\36\u014a\13\36\3\37"+
		"\3\37\3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3%\3&\3&\3&\3\'\3\'\3(\3("+
		"\3(\3)\3)\3*\3*\3*\3+\3+\3+\3,\3,\3,\3-\3-\3.\3.\3/\3/\3\60\3\60\3\60"+
		"\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65\3\66\3\66\3\67\3\67"+
		"\38\38\39\39\3:\3:\3;\3;\3;\7;\u018e\n;\f;\16;\u0191\13;\5;\u0193\n;\3"+
		"<\3<\5<\u0197\n<\3=\3=\3=\7=\u019c\n=\f=\16=\u019f\13=\5=\u01a1\n=\3="+
		"\3=\3=\6=\u01a6\n=\r=\16=\u01a7\5=\u01aa\n=\3>\3>\3>\3>\7>\u01b0\n>\f"+
		">\16>\u01b3\13>\3>\3>\3?\3?\3?\3?\7?\u01bb\n?\f?\16?\u01be\13?\3?\5?\u01c1"+
		"\n?\5?\u01c3\n?\3?\6?\u01c6\n?\r?\16?\u01c7\3?\3?\7?\u01cc\n?\f?\16?\u01cf"+
		"\13?\3?\5?\u01d2\n?\5?\u01d4\n?\5?\u01d6\n?\3?\5?\u01d9\n?\3@\3@\3@\3"+
		"@\7@\u01df\n@\f@\16@\u01e2\13@\3@\5@\u01e5\n@\3@\5@\u01e8\n@\3A\3A\7A"+
		"\u01ec\nA\fA\16A\u01ef\13A\3A\3A\7A\u01f3\nA\fA\16A\u01f6\13A\3A\5A\u01f9"+
		"\nA\3A\5A\u01fc\nA\3B\3B\3B\3B\7B\u0202\nB\fB\16B\u0205\13B\3B\5B\u0208"+
		"\nB\3B\5B\u020b\nB\3C\3C\3D\3D\3D\3D\3D\3D\5D\u0215\nD\3D\5D\u0218\nD"+
		"\3D\3D\3D\6D\u021d\nD\rD\16D\u021e\3D\3D\3D\3D\5D\u0225\nD\3D\3D\3E\3"+
		"E\3E\3E\3E\3E\5E\u022f\nE\3F\3F\3F\3F\7F\u0235\nF\fF\16F\u0238\13F\3F"+
		"\3F\3F\5F\u023d\nF\3F\3F\3F\7F\u0242\nF\fF\16F\u0245\13F\3F\5F\u0248\n"+
		"F\3G\3G\3G\3G\7G\u024e\nG\fG\16G\u0251\13G\3H\3H\3H\3H\3H\3H\7H\u0259"+
		"\nH\fH\16H\u025c\13H\3H\3H\3I\3I\7I\u0262\nI\fI\16I\u0265\13I\3I\3I\3"+
		"J\3J\3\u00a2\2K\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31"+
		"\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65"+
		"\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\64"+
		"g\65i\66k\67m8o9q:s;u<w=y>{?}@\177A\u0081B\u0083C\u0085D\u0087E\u0089"+
		"F\u008bG\u008dH\u008fI\u0091J\u0093K\3\2\35\5\2\13\f\17\17\"\"\4\2\f\f"+
		"\17\17\5\2C\\aac|\6\2\62;C\\aac|\3\2\63;\3\2\62;\4\2GGgg\4\2//~~\t\2$"+
		"$^^ddhhppttvv\6\2\f\f$$GHQQ\4\2\62;aa\4\2NNnn\4\2ZZzz\5\2\62;CHch\6\2"+
		"\62;CHaach\3\2\629\4\2\629aa\4\2DDdd\3\2\62\63\4\2\62\63aa\6\2\f\f\17"+
		"\17))^^\n\2$$))^^ddhhppttvv\3\2\62\65\7\2\f\f$$GHQQ^^\3\2\61\61\3\2,,"+
		"\3\2\'\'\2\u02a0\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13"+
		"\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2"+
		"\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2"+
		"!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3"+
		"\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2"+
		"\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E"+
		"\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2"+
		"\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2"+
		"\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k"+
		"\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2s\3\2\2\2\2u\3\2\2\2\2w\3\2"+
		"\2\2\2y\3\2\2\2\2{\3\2\2\2\2}\3\2\2\2\2\177\3\2\2\2\2\u0081\3\2\2\2\2"+
		"\u0083\3\2\2\2\2\u0085\3\2\2\2\2\u0087\3\2\2\2\2\u0089\3\2\2\2\2\u008b"+
		"\3\2\2\2\2\u008d\3\2\2\2\2\u008f\3\2\2\2\2\u0091\3\2\2\2\2\u0093\3\2\2"+
		"\2\3\u0096\3\2\2\2\5\u009c\3\2\2\2\7\u00aa\3\2\2\2\t\u00b5\3\2\2\2\13"+
		"\u00bb\3\2\2\2\r\u00c3\3\2\2\2\17\u00ca\3\2\2\2\21\u00cf\3\2\2\2\23\u00d3"+
		"\3\2\2\2\25\u00d9\3\2\2\2\27\u00de\3\2\2\2\31\u00e6\3\2\2\2\33\u00ed\3"+
		"\2\2\2\35\u00f1\3\2\2\2\37\u00f7\3\2\2\2!\u00fb\3\2\2\2#\u0101\3\2\2\2"+
		"%\u0104\3\2\2\2\'\u0109\3\2\2\2)\u010e\3\2\2\2+\u0117\3\2\2\2-\u011c\3"+
		"\2\2\2/\u0122\3\2\2\2\61\u0125\3\2\2\2\63\u0128\3\2\2\2\65\u012f\3\2\2"+
		"\2\67\u0136\3\2\2\29\u013a\3\2\2\2;\u0141\3\2\2\2=\u014b\3\2\2\2?\u014d"+
		"\3\2\2\2A\u014f\3\2\2\2C\u0151\3\2\2\2E\u0153\3\2\2\2G\u0155\3\2\2\2I"+
		"\u0157\3\2\2\2K\u015a\3\2\2\2M\u015d\3\2\2\2O\u015f\3\2\2\2Q\u0162\3\2"+
		"\2\2S\u0164\3\2\2\2U\u0167\3\2\2\2W\u016a\3\2\2\2Y\u016d\3\2\2\2[\u016f"+
		"\3\2\2\2]\u0171\3\2\2\2_\u0173\3\2\2\2a\u0176\3\2\2\2c\u0178\3\2\2\2e"+
		"\u017a\3\2\2\2g\u017c\3\2\2\2i\u017e\3\2\2\2k\u0180\3\2\2\2m\u0182\3\2"+
		"\2\2o\u0184\3\2\2\2q\u0186\3\2\2\2s\u0188\3\2\2\2u\u0192\3\2\2\2w\u0196"+
		"\3\2\2\2y\u0198\3\2\2\2{\u01ab\3\2\2\2}\u01d5\3\2\2\2\177\u01da\3\2\2"+
		"\2\u0081\u01e9\3\2\2\2\u0083\u01fd\3\2\2\2\u0085\u020c\3\2\2\2\u0087\u020e"+
		"\3\2\2\2\u0089\u022e\3\2\2\2\u008b\u0230\3\2\2\2\u008d\u0249\3\2\2\2\u008f"+
		"\u0252\3\2\2\2\u0091\u025f\3\2\2\2\u0093\u0268\3\2\2\2\u0095\u0097\t\2"+
		"\2\2\u0096\u0095\3\2\2\2\u0097\u0098\3\2\2\2\u0098\u0096\3\2\2\2\u0098"+
		"\u0099\3\2\2\2\u0099\u009a\3\2\2\2\u009a\u009b\b\2\2\2\u009b\4\3\2\2\2"+
		"\u009c\u009d\7\61\2\2\u009d\u009e\7,\2\2\u009e\u00a2\3\2\2\2\u009f\u00a1"+
		"\13\2\2\2\u00a0\u009f\3\2\2\2\u00a1\u00a4\3\2\2\2\u00a2\u00a3\3\2\2\2"+
		"\u00a2\u00a0\3\2\2\2\u00a3\u00a5\3\2\2\2\u00a4\u00a2\3\2\2\2\u00a5\u00a6"+
		"\7,\2\2\u00a6\u00a7\7\61\2\2\u00a7\u00a8\3\2\2\2\u00a8\u00a9\b\3\2\2\u00a9"+
		"\6\3\2\2\2\u00aa\u00ab\7\'\2\2\u00ab\u00ac\7\'\2\2\u00ac\u00b0\3\2\2\2"+
		"\u00ad\u00af\n\3\2\2\u00ae\u00ad\3\2\2\2\u00af\u00b2\3\2\2\2\u00b0\u00ae"+
		"\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1\u00b3\3\2\2\2\u00b2\u00b0\3\2\2\2\u00b3"+
		"\u00b4\b\4\2\2\u00b4\b\3\2\2\2\u00b5\u00b6\7e\2\2\u00b6\u00b7\7n\2\2\u00b7"+
		"\u00b8\7c\2\2\u00b8\u00b9\7u\2\2\u00b9\u00ba\7u\2\2\u00ba\n\3\2\2\2\u00bb"+
		"\u00bc\7g\2\2\u00bc\u00bd\7z\2\2\u00bd\u00be\7v\2\2\u00be\u00bf\7g\2\2"+
		"\u00bf\u00c0\7p\2\2\u00c0\u00c1\7f\2\2\u00c1\u00c2\7u\2\2\u00c2\f\3\2"+
		"\2\2\u00c3\u00c4\7u\2\2\u00c4\u00c5\7v\2\2\u00c5\u00c6\7c\2\2\u00c6\u00c7"+
		"\7v\2\2\u00c7\u00c8\7k\2\2\u00c8\u00c9\7e\2\2\u00c9\16\3\2\2\2\u00ca\u00cb"+
		"\7v\2\2\u00cb\u00cc\7j\2\2\u00cc\u00cd\7k\2\2\u00cd\u00ce\7u\2\2\u00ce"+
		"\20\3\2\2\2\u00cf\u00d0\7p\2\2\u00d0\u00d1\7g\2\2\u00d1\u00d2\7y\2\2\u00d2"+
		"\22\3\2\2\2\u00d3\u00d4\7h\2\2\u00d4\u00d5\7k\2\2\u00d5\u00d6\7p\2\2\u00d6"+
		"\u00d7\7c\2\2\u00d7\u00d8\7n\2\2\u00d8\24\3\2\2\2\u00d9\u00da\7x\2\2\u00da"+
		"\u00db\7q\2\2\u00db\u00dc\7k\2\2\u00dc\u00dd\7f\2\2\u00dd\26\3\2\2\2\u00de"+
		"\u00df\7d\2\2\u00df\u00e0\7q\2\2\u00e0\u00e1\7q\2\2\u00e1\u00e2\7n\2\2"+
		"\u00e2\u00e3\7g\2\2\u00e3\u00e4\7c\2\2\u00e4\u00e5\7p\2\2\u00e5\30\3\2"+
		"\2\2\u00e6\u00e7\7u\2\2\u00e7\u00e8\7v\2\2\u00e8\u00e9\7t\2\2\u00e9\u00ea"+
		"\7k\2\2\u00ea\u00eb\7p\2\2\u00eb\u00ec\7i\2\2\u00ec\32\3\2\2\2\u00ed\u00ee"+
		"\7k\2\2\u00ee\u00ef\7p\2\2\u00ef\u00f0\7v\2\2\u00f0\34\3\2\2\2\u00f1\u00f2"+
		"\7h\2\2\u00f2\u00f3\7n\2\2\u00f3\u00f4\7q\2\2\u00f4\u00f5\7c\2\2\u00f5"+
		"\u00f6\7v\2\2\u00f6\36\3\2\2\2\u00f7\u00f8\7h\2\2\u00f8\u00f9\7q\2\2\u00f9"+
		"\u00fa\7t\2\2\u00fa \3\2\2\2\u00fb\u00fc\7d\2\2\u00fc\u00fd\7t\2\2\u00fd"+
		"\u00fe\7g\2\2\u00fe\u00ff\7c\2\2\u00ff\u0100\7m\2\2\u0100\"\3\2\2\2\u0101"+
		"\u0102\7k\2\2\u0102\u0103\7h\2\2\u0103$\3\2\2\2\u0104\u0105\7g\2\2\u0105"+
		"\u0106\7n\2\2\u0106\u0107\7u\2\2\u0107\u0108\7g\2\2\u0108&\3\2\2\2\u0109"+
		"\u010a\7v\2\2\u010a\u010b\7j\2\2\u010b\u010c\7g\2\2\u010c\u010d\7p\2\2"+
		"\u010d(\3\2\2\2\u010e\u010f\7e\2\2\u010f\u0110\7q\2\2\u0110\u0111\7p\2"+
		"\2\u0111\u0112\7v\2\2\u0112\u0113\7k\2\2\u0113\u0114\7p\2\2\u0114\u0115"+
		"\7w\2\2\u0115\u0116\7g\2\2\u0116*\3\2\2\2\u0117\u0118\7v\2\2\u0118\u0119"+
		"\7t\2\2\u0119\u011a\7w\2\2\u011a\u011b\7g\2\2\u011b,\3\2\2\2\u011c\u011d"+
		"\7h\2\2\u011d\u011e\7c\2\2\u011e\u011f\7n\2\2\u011f\u0120\7u\2\2\u0120"+
		"\u0121\7g\2\2\u0121.\3\2\2\2\u0122\u0123\7f\2\2\u0123\u0124\7q\2\2\u0124"+
		"\60\3\2\2\2\u0125\u0126\7v\2\2\u0126\u0127\7q\2\2\u0127\62\3\2\2\2\u0128"+
		"\u0129\7f\2\2\u0129\u012a\7q\2\2\u012a\u012b\7y\2\2\u012b\u012c\7p\2\2"+
		"\u012c\u012d\7v\2\2\u012d\u012e\7q\2\2\u012e\64\3\2\2\2\u012f\u0130\7"+
		"t\2\2\u0130\u0131\7g\2\2\u0131\u0132\7v\2\2\u0132\u0133\7w\2\2\u0133\u0134"+
		"\7t\2\2\u0134\u0135\7p\2\2\u0135\66\3\2\2\2\u0136\u0137\7p\2\2\u0137\u0138"+
		"\7k\2\2\u0138\u0139\7n\2\2\u01398\3\2\2\2\u013a\u013b\7c\2\2\u013b\u013c"+
		"\7t\2\2\u013c\u013d\7t\2\2\u013d\u013e\7c\2\2\u013e\u013f\7{\2\2\u013f"+
		":\3\2\2\2\u0140\u0142\t\4\2\2\u0141\u0140\3\2\2\2\u0142\u0143\3\2\2\2"+
		"\u0143\u0141\3\2\2\2\u0143\u0144\3\2\2\2\u0144\u0148\3\2\2\2\u0145\u0147"+
		"\t\5\2\2\u0146\u0145\3\2\2\2\u0147\u014a\3\2\2\2\u0148\u0146\3\2\2\2\u0148"+
		"\u0149\3\2\2\2\u0149<\3\2\2\2\u014a\u0148\3\2\2\2\u014b\u014c\7-\2\2\u014c"+
		">\3\2\2\2\u014d\u014e\7/\2\2\u014e@\3\2\2\2\u014f\u0150\7,\2\2\u0150B"+
		"\3\2\2\2\u0151\u0152\7\61\2\2\u0152D\3\2\2\2\u0153\u0154\7^\2\2\u0154"+
		"F\3\2\2\2\u0155\u0156\7\'\2\2\u0156H\3\2\2\2\u0157\u0158\7#\2\2\u0158"+
		"\u0159\7?\2\2\u0159J\3\2\2\2\u015a\u015b\7?\2\2\u015b\u015c\7?\2\2\u015c"+
		"L\3\2\2\2\u015d\u015e\7>\2\2\u015eN\3\2\2\2\u015f\u0160\7>\2\2\u0160\u0161"+
		"\7?\2\2\u0161P\3\2\2\2\u0162\u0163\7@\2\2\u0163R\3\2\2\2\u0164\u0165\7"+
		"@\2\2\u0165\u0166\7?\2\2\u0166T\3\2\2\2\u0167\u0168\7~\2\2\u0168\u0169"+
		"\7~\2\2\u0169V\3\2\2\2\u016a\u016b\7(\2\2\u016b\u016c\7(\2\2\u016cX\3"+
		"\2\2\2\u016d\u016e\7#\2\2\u016eZ\3\2\2\2\u016f\u0170\7`\2\2\u0170\\\3"+
		"\2\2\2\u0171\u0172\7?\2\2\u0172^\3\2\2\2\u0173\u0174\7<\2\2\u0174\u0175"+
		"\7?\2\2\u0175`\3\2\2\2\u0176\u0177\7}\2\2\u0177b\3\2\2\2\u0178\u0179\7"+
		"\177\2\2\u0179d\3\2\2\2\u017a\u017b\7*\2\2\u017bf\3\2\2\2\u017c\u017d"+
		"\7+\2\2\u017dh\3\2\2\2\u017e\u017f\7]\2\2\u017fj\3\2\2\2\u0180\u0181\7"+
		"_\2\2\u0181l\3\2\2\2\u0182\u0183\7\60\2\2\u0183n\3\2\2\2\u0184\u0185\7"+
		".\2\2\u0185p\3\2\2\2\u0186\u0187\7=\2\2\u0187r\3\2\2\2\u0188\u0189\7<"+
		"\2\2\u0189t\3\2\2\2\u018a\u0193\7\62\2\2\u018b\u018f\t\6\2\2\u018c\u018e"+
		"\t\7\2\2\u018d\u018c\3\2\2\2\u018e\u0191\3\2\2\2\u018f\u018d\3\2\2\2\u018f"+
		"\u0190\3\2\2\2\u0190\u0193\3\2\2\2\u0191\u018f\3\2\2\2\u0192\u018a\3\2"+
		"\2\2\u0192\u018b\3\2\2\2\u0193v\3\2\2\2\u0194\u0197\5-\27\2\u0195\u0197"+
		"\5+\26\2\u0196\u0194\3\2\2\2\u0196\u0195\3\2\2\2\u0197x\3\2\2\2\u0198"+
		"\u01a0\5u;\2\u0199\u019d\7\60\2\2\u019a\u019c\t\7\2\2\u019b\u019a\3\2"+
		"\2\2\u019c\u019f\3\2\2\2\u019d\u019b\3\2\2\2\u019d\u019e\3\2\2\2\u019e"+
		"\u01a1\3\2\2\2\u019f\u019d\3\2\2\2\u01a0\u0199\3\2\2\2\u01a0\u01a1\3\2"+
		"\2\2\u01a1\u01a9\3\2\2\2\u01a2\u01a3\t\b\2\2\u01a3\u01a5\t\t\2\2\u01a4"+
		"\u01a6\t\7\2\2\u01a5\u01a4\3\2\2\2\u01a6\u01a7\3\2\2\2\u01a7\u01a5\3\2"+
		"\2\2\u01a7\u01a8\3\2\2\2\u01a8\u01aa\3\2\2\2\u01a9\u01a2\3\2\2\2\u01a9"+
		"\u01aa\3\2\2\2\u01aaz\3\2\2\2\u01ab\u01b1\7$\2\2\u01ac\u01ad\7^\2\2\u01ad"+
		"\u01b0\t\n\2\2\u01ae\u01b0\n\13\2\2\u01af\u01ac\3\2\2\2\u01af\u01ae\3"+
		"\2\2\2\u01b0\u01b3\3\2\2\2\u01b1\u01af\3\2\2\2\u01b1\u01b2\3\2\2\2\u01b2"+
		"\u01b4\3\2\2\2\u01b3\u01b1\3\2\2\2\u01b4\u01b5\7$\2\2\u01b5|\3\2\2\2\u01b6"+
		"\u01d6\7\62\2\2\u01b7\u01d3\t\6\2\2\u01b8\u01c0\t\7\2\2\u01b9\u01bb\t"+
		"\f\2\2\u01ba\u01b9\3\2\2\2\u01bb\u01be\3\2\2\2\u01bc\u01ba\3\2\2\2\u01bc"+
		"\u01bd\3\2\2\2\u01bd\u01bf\3\2\2\2\u01be\u01bc\3\2\2\2\u01bf\u01c1\t\7"+
		"\2\2\u01c0\u01bc\3\2\2\2\u01c0\u01c1\3\2\2\2\u01c1\u01c3\3\2\2\2\u01c2"+
		"\u01b8\3\2\2\2\u01c2\u01c3\3\2\2\2\u01c3\u01d4\3\2\2\2\u01c4\u01c6\7a"+
		"\2\2\u01c5\u01c4\3\2\2\2\u01c6\u01c7\3\2\2\2\u01c7\u01c5\3\2\2\2\u01c7"+
		"\u01c8\3\2\2\2\u01c8\u01c9\3\2\2\2\u01c9\u01d1\t\7\2\2\u01ca\u01cc\t\f"+
		"\2\2\u01cb\u01ca\3\2\2\2\u01cc\u01cf\3\2\2\2\u01cd\u01cb\3\2\2\2\u01cd"+
		"\u01ce\3\2\2\2\u01ce\u01d0\3\2\2\2\u01cf\u01cd\3\2\2\2\u01d0\u01d2\t\7"+
		"\2\2\u01d1\u01cd\3\2\2\2\u01d1\u01d2\3\2\2\2\u01d2\u01d4\3\2\2\2\u01d3"+
		"\u01c2\3\2\2\2\u01d3\u01c5\3\2\2\2\u01d4\u01d6\3\2\2\2\u01d5\u01b6\3\2"+
		"\2\2\u01d5\u01b7\3\2\2\2\u01d6\u01d8\3\2\2\2\u01d7\u01d9\t\r\2\2\u01d8"+
		"\u01d7\3\2\2\2\u01d8\u01d9\3\2\2\2\u01d9~\3\2\2\2\u01da\u01db\7\62\2\2"+
		"\u01db\u01dc\t\16\2\2\u01dc\u01e4\t\17\2\2\u01dd\u01df\t\20\2\2\u01de"+
		"\u01dd\3\2\2\2\u01df\u01e2\3\2\2\2\u01e0\u01de\3\2\2\2\u01e0\u01e1\3\2"+
		"\2\2\u01e1\u01e3\3\2\2\2\u01e2\u01e0\3\2\2\2\u01e3\u01e5\t\17\2\2\u01e4"+
		"\u01e0\3\2\2\2\u01e4\u01e5\3\2\2\2\u01e5\u01e7\3\2\2\2\u01e6\u01e8\t\r"+
		"\2\2\u01e7\u01e6\3\2\2\2\u01e7\u01e8\3\2\2\2\u01e8\u0080\3\2\2\2\u01e9"+
		"\u01ed\7\62\2\2\u01ea\u01ec\7a\2\2\u01eb\u01ea\3\2\2\2\u01ec\u01ef\3\2"+
		"\2\2\u01ed\u01eb\3\2\2\2\u01ed\u01ee\3\2\2\2\u01ee\u01f0\3\2\2\2\u01ef"+
		"\u01ed\3\2\2\2\u01f0\u01f8\t\21\2\2\u01f1\u01f3\t\22\2\2\u01f2\u01f1\3"+
		"\2\2\2\u01f3\u01f6\3\2\2\2\u01f4\u01f2\3\2\2\2\u01f4\u01f5\3\2\2\2\u01f5"+
		"\u01f7\3\2\2\2\u01f6\u01f4\3\2\2\2\u01f7\u01f9\t\21\2\2\u01f8\u01f4\3"+
		"\2\2\2\u01f8\u01f9\3\2\2\2\u01f9\u01fb\3\2\2\2\u01fa\u01fc\t\r\2\2\u01fb"+
		"\u01fa\3\2\2\2\u01fb\u01fc\3\2\2\2\u01fc\u0082\3\2\2\2\u01fd\u01fe\7\62"+
		"\2\2\u01fe\u01ff\t\23\2\2\u01ff\u0207\t\24\2\2\u0200\u0202\t\25\2\2\u0201"+
		"\u0200\3\2\2\2\u0202\u0205\3\2\2\2\u0203\u0201\3\2\2\2\u0203\u0204\3\2"+
		"\2\2\u0204\u0206\3\2\2\2\u0205\u0203\3\2\2\2\u0206\u0208\t\24\2\2\u0207"+
		"\u0203\3\2\2\2\u0207\u0208\3\2\2\2\u0208\u020a\3\2\2\2\u0209\u020b\t\r"+
		"\2\2\u020a\u0209\3\2\2\2\u020a\u020b\3\2\2\2\u020b\u0084\3\2\2\2\u020c"+
		"\u020d\5\67\34\2\u020d\u0086\3\2\2\2\u020e\u0224\7)\2\2\u020f\u0225\n"+
		"\26\2\2\u0210\u0211\7^\2\2\u0211\u0225\t\27\2\2\u0212\u0217\7^\2\2\u0213"+
		"\u0215\t\30\2\2\u0214\u0213\3\2\2\2\u0214\u0215\3\2\2\2\u0215\u0216\3"+
		"\2\2\2\u0216\u0218\t\21\2\2\u0217\u0214\3\2\2\2\u0217\u0218\3\2\2\2\u0218"+
		"\u0219\3\2\2\2\u0219\u0225\t\21\2\2\u021a\u021c\7^\2\2\u021b\u021d\7w"+
		"\2\2\u021c\u021b\3\2\2\2\u021d\u021e\3\2\2\2\u021e\u021c\3\2\2\2\u021e"+
		"\u021f\3\2\2\2\u021f\u0220\3\2\2\2\u0220\u0221\t\17\2\2\u0221\u0222\t"+
		"\17\2\2\u0222\u0223\t\17\2\2\u0223\u0225\t\17\2\2\u0224\u020f\3\2\2\2"+
		"\u0224\u0210\3\2\2\2\u0224\u0212\3\2\2\2\u0224\u021a\3\2\2\2\u0225\u0226"+
		"\3\2\2\2\u0226\u0227\7)\2\2\u0227\u0088\3\2\2\2\u0228\u022f\5u;\2\u0229"+
		"\u022f\5y=\2\u022a\u022f\5\u0087D\2\u022b\u022f\5{>\2\u022c\u022f\5w<"+
		"\2\u022d\u022f\5\u0085C\2\u022e\u0228\3\2\2\2\u022e\u0229\3\2\2\2\u022e"+
		"\u022a\3\2\2\2\u022e\u022b\3\2\2\2\u022e\u022c\3\2\2\2\u022e\u022d\3\2"+
		"\2\2\u022f\u008a\3\2\2\2\u0230\u0236\7$\2\2\u0231\u0232\7^\2\2\u0232\u0235"+
		"\t\n\2\2\u0233\u0235\n\31\2\2\u0234\u0231\3\2\2\2\u0234\u0233\3\2\2\2"+
		"\u0235\u0238\3\2\2\2\u0236\u0234\3\2\2\2\u0236\u0237\3\2\2\2\u0237\u0239"+
		"\3\2\2\2\u0238\u0236\3\2\2\2\u0239\u023c\7^\2\2\u023a\u023d\n\n\2\2\u023b"+
		"\u023d\7\2\2\3\u023c\u023a\3\2\2\2\u023c\u023b\3\2\2\2\u023d\u0243\3\2"+
		"\2\2\u023e\u023f\7^\2\2\u023f\u0242\t\n\2\2\u0240\u0242\n\31\2\2\u0241"+
		"\u023e\3\2\2\2\u0241\u0240\3\2\2\2\u0242\u0245\3\2\2\2\u0243\u0241\3\2"+
		"\2\2\u0243\u0244\3\2\2\2\u0244\u0247\3\2\2\2\u0245\u0243\3\2\2\2\u0246"+
		"\u0248\7$\2\2\u0247\u0246\3\2\2\2\u0247\u0248\3\2\2\2\u0248\u008c\3\2"+
		"\2\2\u0249\u024f\7$\2\2\u024a\u024b\7^\2\2\u024b\u024e\t\n\2\2\u024c\u024e"+
		"\n\13\2\2\u024d\u024a\3\2\2\2\u024d\u024c\3\2\2\2\u024e\u0251\3\2\2\2"+
		"\u024f\u024d\3\2\2\2\u024f\u0250\3\2\2\2\u0250\u008e\3\2\2\2\u0251\u024f"+
		"\3\2\2\2\u0252\u0253\7\61\2\2\u0253\u0254\7,\2\2\u0254\u025a\3\2\2\2\u0255"+
		"\u0256\7,\2\2\u0256\u0259\n\32\2\2\u0257\u0259\n\33\2\2\u0258\u0255\3"+
		"\2\2\2\u0258\u0257\3\2\2\2\u0259\u025c\3\2\2\2\u025a\u0258\3\2\2\2\u025a"+
		"\u025b\3\2\2\2\u025b\u025d\3\2\2\2\u025c\u025a\3\2\2\2\u025d\u025e\7\2"+
		"\2\3\u025e\u0090\3\2\2\2\u025f\u0263\7\'\2\2\u0260\u0262\n\34\2\2\u0261"+
		"\u0260\3\2\2\2\u0262\u0265\3\2\2\2\u0263\u0261\3\2\2\2\u0263\u0264\3\2"+
		"\2\2\u0264\u0266\3\2\2\2\u0265\u0263\3\2\2\2\u0266\u0267\7\2\2\3\u0267"+
		"\u0092\3\2\2\2\u0268\u0269\13\2\2\2\u0269\u0094\3\2\2\2\64\2\u0098\u00a2"+
		"\u00b0\u0143\u0148\u018f\u0192\u0196\u019d\u01a0\u01a7\u01a9\u01af\u01b1"+
		"\u01bc\u01c0\u01c2\u01c7\u01cd\u01d1\u01d3\u01d5\u01d8\u01e0\u01e4\u01e7"+
		"\u01ed\u01f4\u01f8\u01fb\u0203\u0207\u020a\u0214\u0217\u021e\u0224\u022e"+
		"\u0234\u0236\u023c\u0241\u0243\u0247\u024d\u024f\u0258\u025a\u0263\3\b"+
		"\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}