/**
 * Student name: Le Duy Minh
 * Student ID/Email: minhld.sg@gmail.com
 */
grammar BKOOL;

@lexer::header{
	package bkool.parser;
}

@lexer::members{
@Override
public Token emit() {
    switch (getType()) {
        case ILLEGAL_ESCAPE:
            Token result = super.emit();
            throw new IllegalEscape(result.getText());

        case UNCLOSE_STRING:
            result = super.emit();
            throw new UncloseString(result.getText());

        case ERROR_TOKEN:
            result = super.emit();
            throw new ErrorToken(result.getText());

        case UNTERMINATED_COMMENT:
            result = super.emit();
            throw new UnterminatedComment(result.getText());

        case UNTERMINATED_COMMENT_INLINE:
            result = super.emit();
            throw new UnterminatedComment(result.getText());

        default:
            return super.emit();
        }
    }
}

@parser::header{
	package bkool.parser;
}

options{
	language=Java;
}

program: 'class' ID SPT_LP SPT_RP EOF;

// White spaces
WS : [ \t\r\n]+ -> skip;

// Comments
BC: '/*' .*? '*/' -> skip; // Block comment
LC: '%%' ~[\r\n]* -> skip; // Line comment

// Keywords
KEY_CLASS: 'class';
KEY_EXTENDS: 'extends';
KEY_STATIC: 'static';
KEY_THIS: 'this';
KEY_NEW: 'new';
KEY_FINAL: 'final';
KEY_VOID: 'void';
KEY_BOOLEAN: 'boolean';
KEY_STRING: 'string';
KEY_INT: 'int';
KEY_FLOAT: 'float';
KEY_FOR: 'for';
KEY_BREAK: 'break';
KEY_IF: 'if';
KEY_ELSE: 'else';
KEY_THEN: 'then';
KEY_CONTINUE: 'continue';
KEY_TRUE: 'true';
KEY_FALSE: 'false';
KEY_DO: 'do';
KEY_TO: 'to';
KEY_DOWNTO: 'downto';
KEY_RETURN: 'return';
KEY_NIL: 'nil';

// Identifiers
ID: [a-zA-Z_]+[a-zA-Z_0-9]*;

// Literals
INTERGER_LITERAL: '0'|[1-9][0-9]*;
BOOLEAN_LITERAL: KEY_FALSE | KEY_TRUE;
FLOAT_LITERAL: INTERGER_LITERAL ('.' [0-9]*)? ([eE][|-][0-9]+)?;

// Operators
OPT_ADD: '+';
OPT_SUB: '-';
OPT_MUL: '*';
OPT_FLOAT_DIV: '/';
OPT_INT_DIV: '\\';
OPT_MOD: '%';
OPT_NOTEQUAL: '!=';
OPT_EQUAL: '==';
OPT_LT: '<';
OPT_LE: '<=';
OPT_GT: '>';
OPT_GE: '>=';
OPT_OR: '||';
OPT_AND: '&&';
OPT_BANG: '!';
OPT_CARET: '^';

// Separators
SPT_LP: '{';
SPT_RP: '}';
SPT_LB: '(';
SPT_RB: ')';
SPT_LRSB: '|'; // Open and Close square bracket
SPT_DOT: '.';
SPT_COM: ',';
SPT_SCOL: ';';
SPT_COL: ':';

// Exceptions
ILLEGAL_ESCAPE: '"'('\\'[bfrnt\\"]|~[\n\\"EOF])*('\\'(~[bfrnt\\"]|EOF))('\\'[bfrnt\\"]|~[\n\\"EOF])*'"'?;
UNCLOSE_STRING: '"'('\\'[bfrnt\\"]|~[\n"EOF])*;
STRING_LITERAL: '"'('\\'[bfrnt\\"]|~[\n"EOF])*'"';
UNTERMINATED_COMMENT: '/*'('*'~'/'|~'*')*EOF;
UNTERMINATED_COMMENT_INLINE: '%'(~'%')*EOF;

ERROR_TOKEN: .;
